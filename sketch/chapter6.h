//
// Created by Volodymyr Samoilenko on 2019-07-30.
//
#ifndef RAYTRACER_CHALLENGE_CHAPTER6_H
#define RAYTRACER_CHALLENGE_CHAPTER6_H

#include "../renderer/Canvas.h"
#include "../renderer/components/Material.h"
#include "../renderer/components/Sphere.h"


class chapter6 {
public:
    static void run();
};


#endif //RAYTRACER_CHALLENGE_CHAPTER6_H
