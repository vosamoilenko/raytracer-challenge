//
// Created by Volodymyr Samoilenko on 2019-07-07.
//
#include "projectile.h"

// PROJECTILE
struct Projectile {
    Projectile(P3 p, V3 v) {
        pos = p;
        vel = v;
    }

    P3 pos;
    V3 vel;
};

struct Env {
    Env(V3 g, V3 w) {
        grav = g;
        wind = w;
    }

    V3 grav;
    V3 wind;
};

Projectile tick(Projectile p, Env e) {
    XYZW pos = p.pos + p.vel;
    XYZW vel = p.vel + e.grav + e.wind;

    return Projectile(pos.values, vel.values);
}

vector<P3> getProjectiles(Projectile projectile, Env env) {
    int counter = 0;
    vector<P3> out;

    while (projectile.pos.y() >= 0) {
        projectile = tick(projectile, env);
        counter += 1;
        out.push_back(projectile.pos);
    }
    return out;
}

void run() {
    int height = 550;
    Canvas canvas(900, height);
    P3 start(0, 1, 0);
    V3 velocity = V3(1, 1.8, 0).normalize() * 11.25;
    Projectile projectile(start, velocity);

    V3 gravity(0, -0.1, 0);
    V3 wind(-0.01, -0.1, 0);
    Env env(gravity, wind);

    vector<P3> points = getProjectiles(projectile, env);
    P3 temp(0, 0, 0);

    // align center x
    for (int i = 0; i < points.size(); i++) {
        P3 point = points.at(i);
        if (point.x() > temp.x()) {
            temp = point;
        }
    }

    int offset = (900 - temp.x()) / 2;

    for (auto point : points) {
        int x = point.x() + offset;
        int y = height - point.y();

        if (x < 0) {
            x = 0;
        } else if (x > 900 - 1) {
            x = 900 - 1;
        }

        if (y < 0) {
            y = 0;
        } else if (y > 550 - 1) {
            y = 550 - 1;
        }
        canvas.write(x, y, Color(1, 0, 0));
    }

    canvas.ppm("projectile");
}