//
// Created by Volodymyr Samoilenko on 2019-07-19.
//

#ifndef RAYTRACER_CHALLENGE_CLOCK_H
#define RAYTRACER_CHALLENGE_CLOCK_H

#include "../renderer/math/Matrix.h"
#include "../renderer/Canvas.h"

class clock {
    void drawCircle(Point startPoint, double radius, Canvas &canvas, bool n);
    void run();
};


#endif //RAYTRACER_CHALLENGE_CLOCK_H
