//
// Created by Volodymyr Samoilenko on 2019-07-07.
//

#ifndef RAYTRACER_CHALLENGE_PROJECTILE_H
#define RAYTRACER_CHALLENGE_PROJECTILE_H

#include <iostream>
#include <algorithm>
#include <vector>

#include "../renderer/math/Color.h"
#include "../renderer/math/Point.h"
#include "../renderer/math/Vector.h"
#include "../renderer/Canvas.h"

using namespace std;

// PROJECTILE
struct Projectile;

struct Env;

Projectile tick(Projectile p, Env e);

vector<P3> getProjectiles(Projectile projectile, Env env);

void run();
#endif //RAYTRACER_CHALLENGE_PROJECTILE_H
