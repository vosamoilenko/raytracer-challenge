//
// Created by Volodymyr Samoilenko on 2019-07-27.
//

#include "circle.h"

#include "../renderer/components/Sphere.h"
#include "../renderer/Canvas.h"

void circle::run()
{
    const int size = 100;
    Canvas canvas(size, size);
    Point rayOrigin(0, 0, -5);
    Sphere s;
    Matrix m = (*Matrix::rotationZMatrix(M_PI / 4) * *Matrix::shearingMatrix(1, 0, 0, 0, 0, 0) * *Matrix::scalingMatrix(0.5, 1, 1));
    s.transformationMatrix = &m;
    Color c;
    double wallZ = 7;

    double half = wallZ / 2.0;
    double pixelS = wallZ / size;
    for (int y = 0; y < size; y++)
    {
        double v = half - pixelS * y;
        for (int x = 0; x < size; x++)
        {
            double u = -half + (pixelS * x);
            Point point(u, v, wallZ);
            Vector direction = (point - rayOrigin).normalize();
            Ray ray(rayOrigin, direction);
            Intersections intersect = ray.intersect(s);

            if (intersect.size() != 0)
            {
                c = Color(255, 0, 0);
                canvas.write(x, y, c);
            }
        }
    }

    canvas.ppm("circle");
}
