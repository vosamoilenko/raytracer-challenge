//
// Created by Volodymyr Samoilenko on 2019-07-30.
//

#include "chapter6.h"

Light produceLight() {
    Point lightPosition(-10, 10, -10);
    Color lightColor(1, 1, 1);
    Light pointLight(lightPosition, lightColor);
    return pointLight;
}

Sphere produceSphere() {
    Material *m = new Material(Color(1, 0.2, 1));
    Sphere s;
    s.material = m;
    return s;
}

void chapter6::run() {
    const int size = 500;
    Canvas canvas(size, size);
    Sphere s = produceSphere();
    Light pointLight = produceLight();

    Point rayOrigin(0, 0, -5);

    double wallZ = 7;

    double half = wallZ / 2.0;
    double pixelS = wallZ / size;
    int counter = 0;
    for (int y = 0; y < size; y++) {
        for (int x = 0; x < size; x++, counter++) {
            double v = half - pixelS * y;
            double u = -half + (pixelS * x);

            Point point(u, v, wallZ);
            Vector direction = (point - rayOrigin).normalize();
            Ray ray(rayOrigin, direction);
            Intersections intersections = ray.intersect(s);

            if (intersections.size() > 0) {
                Intersection hit = *ray.hit(intersections);
                Point p = ray.position(hit.t);
                Sphere sp = *hit.object;
                Vector normalAtSphere = sp.normalAt(p);
                Vector eye = ray.direction * -1;
                Color out = Material::lightning(*sp.material, pointLight, point, eye, normalAtSphere);
                canvas.write(x, y, out);
            }
        }
    }

    canvas.ppm("chapter6");
}
