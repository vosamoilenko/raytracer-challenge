//
// Created by Volodymyr Samoilenko on 2019-07-19.
//

#include "clock.h"

void clock::drawCircle(Point startPoint, double radius, Canvas &canvas, bool n) {
    int startAngle = 0;
    while (startAngle <= 360) {
        Point r = *Matrix::rotationZMatrix((startAngle * M_PI) / 180.0) * *Matrix::translationMatrix(0, radius, 0) *
                  startPoint;
        int newX = int(r.x() + canvas.w() / 2);
        int newY = int(r.y() + canvas.h() / 2);
        if (n) {
            drawCircle(r, 10, canvas, false);
        }
        canvas.write(newX, newY, Color(1, 1, 1));
        startAngle += 30;
    }
}

void clock::run() {
    Canvas canvas(320, 320);
    const Point center(150, 150, 0);
    const double radius = 100;
    drawCircle(Point(0, 0, 0), 100, canvas, true);

    canvas.ppm("clock");
}
