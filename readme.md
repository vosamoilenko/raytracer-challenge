# How to run


# CATCH Testing Library

### `-t` tags
```
TEST_CASE("name", "[tag],[tag],[tag]") {
```

## Keywords

* TEST_CASE
```
TEST_CASE(<name>, <tag>) {}
```
* REQUIRE
```
REQUIRE(<expression>);
```
* SECTION
```
SECTION(<name>, <tag>) {}
```
* SCENARIO
```
SCENARIO(<name>, <tag>) {}
```
* GIVEN
```
GIVEN(<description>) {}
```
* WHEN
```
WHEN(<description>) {}
```
* WHEN ... AND_WHEN
```
WHEN(<description>) {
  AND_WHEN(<description>) {}
}
```
* THEN
```
THEN(<description>) {}
```

