//
// Created by Volodymyr Samoilenko on 2019-07-23.
//

#ifndef RAYTRACER_CHALLENGE_INTERSECTION_H
#define RAYTRACER_CHALLENGE_INTERSECTION_H

#include "./Sphere.h"

class Sphere;

class Intersection {
public:
    double t;
    Sphere* object;

    Intersection(double, Sphere*);

    bool operator==(const Intersection&) const;
};


#endif //RAYTRACER_CHALLENGE_INTERSECTION_H
