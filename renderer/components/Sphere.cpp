//
// Created by Volodymyr Samoilenko on 2019-07-19.
//

#include "Sphere.h"

Sphere::Sphere() {
    origin = Point(0,0,0);
    radius = 1;
    transformationMatrix = Matrix::identity();
    material = new Material();
}

Sphere::Sphere(Point origin, double radius) {
    this->origin = origin;
    this->radius = radius;
    transformationMatrix = Matrix::identity();
    material = new Material();
}

Vector Sphere::normalAt(const Point& p) {
    Matrix inverse = transformationMatrix->inverse();
    Point objectPoint = inverse * p;

    Vector objectNormal = objectPoint - origin;
    Vector worldNormal = Matrix::transponse(inverse) * objectNormal;

    worldNormal.values.at(3) = 0;

    return worldNormal.normalize();
}

ostream &operator<<(ostream &out, const Sphere &v) {
    out << "Sphere(or: " << v.origin << ", r: " << v.radius << ", m: " << *v.material <<")";
    return out;
}