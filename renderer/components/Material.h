//
// Created by Volodymyr Samoilenko on 2019-07-28.
//

#ifndef RAYTRACER_CHALLENGE_MATERIAL_H
#define RAYTRACER_CHALLENGE_MATERIAL_H

#include <cmath>

#include "../math/Color.h"
#include "../math/Point.h"
#include "../math/Vector.h"
#include "../components/Light.h"


class Material {
public:
    Color color;
    double ambient;
    double diffuse;
    double specular;
    double shininess;

    Material();
    Material(const Color&);

    bool operator==(Material);
    const bool operator==(const Material&) const;

    static Color lightning(Material material, Light light, Point position, Vector eye, Vector normal);
};

ostream &operator<<(ostream &out, const Material &v);


#endif //RAYTRACER_CHALLENGE_MATERIAL_H
