//
// Created by Volodymyr Samoilenko on 2019-07-19.
//

#ifndef RAYTRACER_CHALLENGE_RAY_H
#define RAYTRACER_CHALLENGE_RAY_H

#include "../math/Matrix.h"
#include "../math/Point.h"
#include "./Sphere.h"
#include "./Intersections.h"

class Matrix;

class Intersections;

class Intersection;

class Sphere;

class Ray {
public:
    Point origin;
    Vector direction;

    Ray();

    Ray(Point, Vector);

    Point position(double);

    Intersections intersect(Sphere &);

    static Intersection *hit(Intersections);

    static Ray *transform(const Ray &, const Matrix &);

};

ostream &operator<<(ostream &out, const Ray &v);


#endif //RAYTRACER_CHALLENGE_RAY_H
