//
// Created by Volodymyr Samoilenko on 2019-07-28.
//

#include "Material.h"

Material::Material() {
    ambient = 0.1;
    diffuse = 0.9;
    specular = 0.9;
    shininess = 200;
    color = Color(1, 1, 1);
}

Material::Material(const Color &c) {
    color = c;
    ambient = 0.1;
    diffuse = 0.9;
    specular = 0.9;
    shininess = 200;
}
const bool Material::operator==(const Material& m) const {
    return m.color == color && m.ambient == ambient && m.diffuse == diffuse && m.specular == specular &&
           m.shininess == shininess;
}

bool Material::operator==(Material m)  {
    return m.color == color && m.ambient == ambient && m.diffuse == diffuse && m.specular == specular &&
           m.shininess == shininess;
}

Color Material::lightning(Material material, Light light, Point point, Vector eye, Vector normal) {

    Color black(0,0,0);
    Color effectiveColor = Color(material.color * light.intensity);
    Vector lightDirection = (light.position - point).normalize();
    Color ambient, diffuse, specular;
    ambient = effectiveColor * material.ambient;
    double dotLightNormal = Vector::dot(lightDirection, normal);
    if (dotLightNormal < 0) {
        diffuse = black;
        specular = black;
    } else {
        diffuse = effectiveColor * material.diffuse * dotLightNormal;

        Vector reflected = Vector::reflect(lightDirection * -1, normal);
        double reflectedDotEye = Vector::dot(reflected, eye);
        if (reflectedDotEye < 0) {
            specular = black;
        } else {
            double factor = pow(reflectedDotEye, material.shininess);
            specular = light.intensity * material.specular * factor;
        }
    }
    return ambient + diffuse + specular;
}

ostream &operator<<(ostream &out, const Material &v) {
    out << "Material(Color: " << v.color << "" << ")";
    return out;
}