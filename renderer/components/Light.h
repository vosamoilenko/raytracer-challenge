//
// Created by Volodymyr Samoilenko on 2019-07-28.
//

#ifndef RAYTRACER_CHALLENGE_LIGHT_H
#define RAYTRACER_CHALLENGE_LIGHT_H


#include "../math/Color.h"
#include "../math/Point.h"

class Light {
public:
    Color intensity;
    Point position;

    Light();
    Light(const Point&, const Color&);
};

ostream &operator<<(ostream &out, const Light &v);

#endif //RAYTRACER_CHALLENGE_LIGHT_H
