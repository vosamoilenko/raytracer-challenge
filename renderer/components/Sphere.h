//
// Created by Volodymyr Samoilenko on 2019-07-19.
//

#ifndef RAYTRACER_CHALLENGE_SPHERE_H
#define RAYTRACER_CHALLENGE_SPHERE_H

#include "../math/Point.h"
#include "../math/Matrix.h"
#include "./Material.h"

class Material;

class Matrix;

class Sphere {
public:
    Point origin;
    double radius;
    Matrix *transformationMatrix;
    Material *material;

    Sphere();

    Sphere(Point, double);

    Vector normalAt(const Point &);
};

ostream &operator<<(ostream &out, const Sphere &v);

#endif //RAYTRACER_CHALLENGE_SPHERE_H
