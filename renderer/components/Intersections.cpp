//
// Created by Volodymyr Samoilenko on 2019-07-23.
//

#include "Intersections.h"

Intersections::Intersections() {}

Intersections::Intersections(vector<Intersection> values) {
    this->values = values;
}

void Intersections::push(Intersection intersection) {
    this->values.push_back(intersection);
}

void Intersections::push(vector<Intersection> values) {
    this->values.insert(this->values.begin(), values.begin(), values.end());
}

const int Intersections::size() const {
    return  this->values.size();
}

Intersection Intersections::operator[](int i) const {
    return this->values.at(i);
}

Intersection& Intersections::operator[](int i) {
    return this->values.at(i);
}

Intersection Intersections::at(int i) const {
    return this->values.at(i);
}
Intersection& Intersections::at(int i) {
    return this->values.at(i);
}