//
// Created by Volodymyr Samoilenko on 2019-07-19.
//

#include "Ray.h"

Ray::Ray() {
    origin = Point(0, 0, 0);
    direction = Vector(0, 0, 0);
}

Ray::Ray(Point origin, Vector direction) {
    this->origin = origin;
    this->direction = direction;
}

Point Ray::position(double t) {
    return this->origin + this->direction * t;
}

Intersections Ray::intersect(Sphere &sphere) {
    Intersections intersections;
    Ray ray = Matrix::inverse(*sphere.transformationMatrix) * *this;

    const Vector CRo = ray.origin - sphere.origin;
    double a = Vector::dot(ray.direction, ray.direction);
    double b = 2 * Vector::dot(ray.direction, CRo);
    double c = Vector::dot(CRo, CRo) - 1;
    double discriminant = b * b - 4 * a * c;
    if (discriminant < 0) {
        return intersections;
    }

    double t1 = (-b - sqrt(discriminant)) / (2 * a);
    double t2 = (-b + sqrt(discriminant)) / (2 * a);

    const Intersection i1(t1, &sphere);
    const Intersection i2(t2, &sphere);
    intersections.push({i1, i2});

    return intersections;
}

Intersection *Ray::hit(Intersections intersections) {
    double smallestTime = numeric_limits<double>::infinity();
    Intersection *intersection = nullptr;
    for (size_t i = 0; i < intersections.size(); i++) {
        const double t = intersections.at(i).t;
        if (t >= 0 && t < smallestTime) {
            smallestTime = t;
            intersection = &intersections.at(i);
        }
    }
    return intersection;
}

Ray *Ray::transform(const Ray &r, const Matrix &m) {
    const Point newOrigin = m * r.origin;
    const Vector newDirection = m * r.direction;
    Ray *ray = new Ray(newOrigin, newDirection);
    return ray;
}

ostream &operator<<(ostream &out, const Ray &v) {
    return out << "Ray(" << v.origin << ", " << v.direction << ")";
}