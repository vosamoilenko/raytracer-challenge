//
// Created by Volodymyr Samoilenko on 2019-07-23.
//

#ifndef RAYTRACER_CHALLENGE_INTERSECTIONS_H
#define RAYTRACER_CHALLENGE_INTERSECTIONS_H

#include "./Intersection.h"
#include <vector>

class Intersection;

using namespace std;

class Intersections {
public:
    vector<Intersection> values;

    Intersections();
    Intersections(vector<Intersection>);

    void push(Intersection);
    void push(vector<Intersection>);

    const int size() const;

    Intersection operator[](int) const;
    Intersection &operator [](int);

    Intersection at(int) const;
    Intersection& at(int);
};

ostream &operator<<(ostream &out, const Intersection &v);


#endif //RAYTRACER_CHALLENGE_INTERSECTIONS_H
