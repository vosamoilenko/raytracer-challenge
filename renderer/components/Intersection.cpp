//
// Created by Volodymyr Samoilenko on 2019-07-23.
//

#include "Intersection.h"

Intersection::Intersection(double t, Sphere* object) {
    this->t = t;
    this->object = object;
}

bool Intersection::operator==(const Intersection& i) const {
    return this->t == i.t && this->object == i.object;
}

ostream &operator<<(ostream &out, const Intersection &v) {
    out << "Interssection(t: " << v.t << ", ptr: " << v.object;
    return out;
}
