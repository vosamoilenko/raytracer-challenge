//
// Created by Volodymyr Samoilenko on 2019-07-28.
//

#include "Light.h"
Light::Light() {}
Light::Light(const Point& p, const Color& c) {
    position = p;
    intensity = c;
}

ostream &operator<<(ostream &out, const Light &v) {
    return out << "Light(Position:" << v.position << ")";
}