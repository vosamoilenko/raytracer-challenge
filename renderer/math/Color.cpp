//
// Created by Volodymyr Samoilenko on 2019-07-06.
//

#include <string>

#include "Color.h"

Color::Color() {
    values = vector<double>{0, 0, 0, 0};
}

Color::Color(double r, double g, double b) {
    values = vector<double>{r, g, b, 0};
}

Color::Color(const Tupel& t) {
    values = t.values;
}

Color::Color(vector<double> rhs) {
    if (rhs.size() != 3 && rhs.size() != 4) {
        string error = "point size is not 3 || 4, current size: " + to_string(rhs.size());
        throw runtime_error(error.c_str());
    }

    values.clear();
    values = vector(rhs.begin(), rhs.end());

    if (rhs.size() == 3) {
        // Marked as a vector
        values.push_back(0);
    }

    if (values.size() != 4) {
        throw runtime_error("Tupel size is not 4");
    }
}

const Color Color::to255() const {
    Tupel temp = (*this * 255.9).toRange(RGB_MIN_VALUE, RGB_MAX_VALUE);
    return Color(temp.x(), temp.y(), temp.z());
}