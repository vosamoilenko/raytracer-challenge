//
// Created by Volodymyr Samoilenko on 2019-07-06.
//

#ifndef RAYTRACER_CHALLENGE_VECTOR_H
#define RAYTRACER_CHALLENGE_VECTOR_H

#include "Tupel.h"

class Vector : public Tupel
{
public:
    Vector();
    Vector(const Tupel &);
    Vector(double x, double y, double z);
    Vector(vector<double>);
    const Vector operator-(Vector &) const;

    Vector normalize() const;
    static Vector reflect(const Vector& in,const Vector& normal);
};

ostream &operator<<(ostream &out, const Vector &v);

#endif //RAYTRACER_CHALLENGE_VECTOR_H
