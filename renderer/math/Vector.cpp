//
// Created by Volodymyr Samoilenko on 2019-07-06.
//

#include <string>

#include "Vector.h"

Vector::Vector() {
    values = vector<double>{0.0, 0.0, 0.0, 0.0};
}
Vector Vector::normalize() const {
    Tupel temp = Tupel::normalize();
    return Vector(temp.x(), temp.y(), temp.z());
}

Vector Vector::reflect(const Vector& in,const Vector& normal) {
    Vector v(normal * 2 * in.dot(normal));
    const Vector reflected = in - v;
    return reflected;
}

Vector::Vector(double x, double y, double z) {
    values = vector<double>{x, y, z, 0.0};
}

Vector::Vector(const Tupel &rhs) {
    values.at(0) = rhs.x();
    values.at(1) = rhs.y();
    values.at(2) = rhs.z();
}

Vector::Vector(vector<double> rhs) {
    if (rhs.size() != 3 && rhs.size() != 4) {
        string error = "point size is not 3 || 4, current size: " + to_string(rhs.size());
        throw runtime_error(error.c_str());
    }

    values.clear();
    values = vector(rhs.begin(), rhs.end());

    if (rhs.size() == 3) {
        // Marked as a vector
        values.push_back(0);
    }

    if (values.size() != 4) {
        throw runtime_error("Tupel size is not 4");
    }
}

const Vector Vector::operator-(Vector &rhs) const {
    Tupel temp = Tupel(*this) - Tupel(rhs);
    return Vector(vector<double>{temp.x(), temp.y(), temp.z()});
}

ostream &operator<<(ostream &out, const Vector &v) {
    string str =  "(" + to_string(v.x()) + "," +to_string(v.y()) + "," + to_string(v.z()) + "," + to_string(v.w()) + ")";
    return out << str;
}