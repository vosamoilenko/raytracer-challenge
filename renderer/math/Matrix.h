//
// Created by Volodymyr Samoilenko on 2019-07-07.
//

#ifndef RAYTRACER_CHALLENGE_MATRIX_H
#define RAYTRACER_CHALLENGE_MATRIX_H

#include <vector>
#include <string>
#include <iostream>
#include <cmath>

#include "../components/Ray.h"
#include "Vector.h"
#include "Point.h"

/**
 * Always first y then x
 */

using namespace std;
class Ray;
class Matrix {
public:
    int rows;
    int columns;
    vector<vector<double>> values;

    Matrix();

    Matrix(int rows, int columns);

    Matrix(int rows, int columns, vector<vector<double>>);

    void operator=(const Matrix &m);

    // A matrix with m rows and n columns is called an m × n matrix or m - by - n matrix
    bool operator!=(Matrix) const;

    bool operator==(Matrix) const;

    const double at(int, int) const;

    void insert(int, int, double);

    Matrix operator*(Matrix rhs) const;

    Matrix operator/(double divider) const;

    Tupel operator*(Tupel t) const;

    Point operator*(Point p) const;

    Vector operator*(Vector v) const;

    Ray operator*(const Ray &r) const;

    Matrix submatrix(int, int);

    Matrix transponse();
    
    static Matrix transponse(Matrix &);

    Matrix translate(double, double, double);

    Matrix scale(double, double, double);

    Matrix rotateX(double);

    Matrix rotateY(double);

    Matrix rotateZ(double);

    Matrix shearing(double, double, double, double, double, double);

    static bool isInvertable(Matrix);

    static double cofactor(int, int, Matrix);

    static double determinant(Matrix);

    static double minor(int, int, Matrix);

    Matrix inverse();

    static Matrix inverse(Matrix);

    static Matrix submatrix(int, int, Matrix);

    static Matrix* identity();

    static Matrix* translationMatrix(double, double, double);

    static Matrix* scalingMatrix(double, double, double);

    static Matrix* rotationXMatrix(double);

    static Matrix* rotationYMatrix(double);

    static Matrix* rotationZMatrix(double);

    static Matrix* shearingMatrix(double, double, double, double, double, double);
};

ostream &operator<<(ostream &out, const Matrix &m);

#endif //RAYTRACER_CHALLENGE_MATRIX_H
