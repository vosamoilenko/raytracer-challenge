//
// Created by Volodymyr Samoilenko on 2019-07-06.
//

#ifndef RAYTRACER_CHALLENGE_TUPEL_H
#define RAYTRACER_CHALLENGE_TUPEL_H

#include <vector>
#include <math.h>

using namespace std;

class Tupel {

public:
    Tupel();

    Tupel(const Tupel &);

    Tupel(double, double, double, double);

    Tupel(vector<double>);

    void operator=(const Tupel &);

    ~Tupel();

    vector<double> values;

    const double x() const;

    const double y() const;

    const double z() const;

    const double w() const;

    const double r() const;

    const double g() const;

    const double b() const;

    const bool isVector();

    const bool isPoint();

    const bool isColor();

    Tupel operator+(Tupel) const;

    Tupel operator-(Tupel) const;

    Tupel operator*(Tupel) const;

    Tupel operator/(Tupel) const;

    // hadamard product
    // https://en.wikipedia.org/wiki/Hadamard_product_(matrices)
    Tupel operator*(double) const;

    Tupel operator/(double) const;

    bool operator==(Tupel) const;

    bool operator==(vector<double>) const;

    Tupel operator-() const;

    double magnitude() const;

    double length() const;

    static double length(const Tupel&);

    Tupel normalize() const;

    // The smaller the dot product, the larger the angle between the vectors.
    // For example, given two unit vectors, a dot product of 1 means the vectors
    // are identical, and a dot product of -1 means they point in opposite directions.
    // More specifically, and again if the two vectors are unit vectors, the dot
    // product is actually the cosine of the angle between them
    double dot(const Tupel&) const;

    static double dot(const Tupel&, const Tupel&);

    Tupel cross(const Tupel&);

    Tupel toRange(double min, double max);

    friend ostream & operator << (ostream &out, const Tupel&);
};

#endif //RAYTRACER_CHALLENGE_TUPEL_H
