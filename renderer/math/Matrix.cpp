//
// Created by Volodymyr Samoilenko on 2019-07-07.
//

#include "Matrix.h"

Matrix::Matrix() {
    this->rows = 0;
    this->columns = 0;
}

Matrix::Matrix(int rows, int columns) {
    this->rows = rows;
    this->columns = columns;
    values = vector(columns, vector<double>(rows, 0));
}

Matrix::Matrix(int rows, int columns, vector<vector<double>> v) {
    this->rows = rows;
    this->columns = columns;

    try {
        if (v.size() != this->rows) {
            throw runtime_error("columns amount is wrong");
        }

        for (int y = 0; y < this->rows; y++) {
            if (v.at(y).size() != this->columns) {
                throw runtime_error("rows amount is wrong");
            }
        }
    }
    catch (const exception &e) {
        throw e;
    }

    values = v;
}

void Matrix::operator=(const Matrix &m) {
    rows = m.rows;
    columns = m.columns;

    values = m.values;
}

Matrix *Matrix::identity() {
    return new Matrix(4, 4, {{1, 0, 0, 0},
                             {0, 1, 0, 0},
                             {0, 0, 1, 0},
                             {0, 0, 0, 1}});
}

const double Matrix::at(int y, int x) const {
    return values.at(y).at(x);
}

void Matrix::insert(int y, int x, double target) {
    values.at(y).at(x) = target;
}

bool Matrix::operator==(Matrix m) const {
    const float EPSILON = 0.00001;
    try {
        if (this->columns != m.columns || this->rows != m.rows) {
            return false;
        }
        if (this->values.size() != m.values.size()) {
            return false;
        }

        for (int y = 0; y < this->values.size(); y++) {
            for (int x = 0; x < this->values.at(y).size(); x++) {
                if (this->values.at(y).size() != m.values.at(y).size()) {
                    return false;
                }

                if (abs(this->at(y, x) - m.at(y, x)) > EPSILON) {
                    return false;
                }
            }
        }
    }
    catch (const exception &e) {
        return false;
    }
    return true;
}

bool Matrix::operator!=(Matrix m) const {
    return !(*this == m);
}

Matrix Matrix::operator*(Matrix m) const {
    if (this->columns != m.rows) {
        throw runtime_error("Unable to multiply matricies");
    }
    Matrix out(this->rows, m.columns);
    for (int y = 0; y < out.rows; y++) {
        for (int x = 0; x < out.columns; x++) {
            double resultValue = 0;
            vector<double> row, column;
            row = this->values.at(y);

            for (int i = 0; i < m.rows; i++) {
                column.push_back(m.at(i, x));
            }

            for (int i = 0; i < row.size(); i++) {
                resultValue += row.at(i) * column.at(i);
            }
            out.insert(y, x, resultValue);
        }
    }
    return out;
}

Tupel Matrix::operator*(Tupel t) const {
    if (this->columns != 4) {
        throw runtime_error("Unable to multiply matricies");
    }

    Tupel out;
    for (int y = 0; y < 4; y++) {

        Tupel row(this->values.at(y));
        out.values.at(y) = row.dot(t);
    }
    return out;
}

Point Matrix::operator*(Point p) const {
    if (this->columns != 4) {
        throw runtime_error("Unable to multiply matrix with a point");
    }

    Point out;
    for (int y = 0; y < 4; y++) {

        Tupel row(this->values.at(y));
        out.values.at(y) = row.dot(p);
    }
    return out;
}

Vector Matrix::operator*(Vector v) const {
    if (this->columns != 4) {
        throw runtime_error("Unable to multiply matrix with a vector");
    }

    Point out;
    for (int y = 0; y < 4; y++) {

        Tupel row(this->values.at(y));
        out.values.at(y) = row.dot(v);
    }
    return out;
}

Ray Matrix::operator*(const Ray &r) const {
    Point newOrigin = *this * r.origin;
    Vector newDirection = *this * r.direction;
    return Ray(newOrigin, newDirection);
}

Matrix Matrix::transponse() {
    Matrix out(this->columns, this->rows);

    for (int y = 0; y < this->rows; y++) {
        for (int x = 0; x < this->columns; x++) {
            out.insert(x, y, this->at(y, x));
        }
    }
    return out;
}

Matrix Matrix::transponse(Matrix &m) {
    return m.transponse();
}


Matrix Matrix::submatrix(int row, int column, Matrix m) {
    Matrix out = m;
    out.rows -= 1;
    out.columns -= 1;

    out.values.erase(out.values.begin() + row);
    for (int y = 0; y < out.values.size(); y++) {
        out.values.at(y).erase(out.values.at(y).begin() + column);
    }
    return out;
}

Matrix Matrix::submatrix(int row, int column) {
    return Matrix::submatrix(row, column, *this);
}

double Matrix::minor(int row, int column, Matrix m) {
    return Matrix::determinant(m.submatrix(row, column));
}

double Matrix::cofactor(int row, int column, Matrix m) {
    // https://stackoverflow.com/a/3114161
    return (row + column) % 2 == 0 ? Matrix::minor(row, column, m) : -Matrix::minor(row, column, m);
}

double Matrix::determinant(Matrix m) {
    double out = 0;
    if (m.rows == 2 && m.columns == 2) {
        return (m.at(0, 0) * m.at(1, 1)) - (m.at(1, 0) * m.at(0, 1));
    }

    for (int i = 0; i < m.columns; i++) {
        double value = m.at(0, i);
        double cofactor = Matrix::cofactor(0, i, m);
        out += value * cofactor;
    }
    return out;
}

bool Matrix::isInvertable(Matrix m) {
    return Matrix::determinant(m) != 0;
}

Matrix Matrix::operator/(double divider) const {
    Matrix out(rows, columns);
    for (int y = 0; y < rows; y++) {
        for (int x = 0; x < columns; x++) {
            out.insert(y, x, at(y, x) / divider);
        }
    }
    return out;
}

Matrix Matrix::inverse(Matrix m) {
    Matrix cofactorMatrix(m.rows, m.columns);

    for (int y = 0; y < m.rows; y++) {
        for (int x = 0; x < m.columns; x++) {
            cofactorMatrix.insert(y, x, Matrix::cofactor(y, x, m));
        }
    }

    cofactorMatrix = cofactorMatrix.transponse();
    return cofactorMatrix / Matrix::determinant(m);
}

Matrix *Matrix::translationMatrix(double x, double y, double z) {
    return new Matrix(4, 4, {{1, 0, 0, x},
                             {0, 1, 0, y},
                             {0, 0, 1, z},
                             {0, 0, 0, 1}});
}

Matrix *Matrix::scalingMatrix(double x, double y, double z) {
    return new Matrix(4, 4, {{x, 0, 0, 0},
                             {0, y, 0, 0},
                             {0, 0, z, 0},
                             {0, 0, 0, 1}});
}

Matrix *Matrix::rotationXMatrix(double rad) {
    return new Matrix(4, 4, {
            {1, 0,        0,         0},
            {0, cos(rad), -sin(rad), 0},
            {0, sin(rad), cos(rad),  0},
            {0, 0,        0,         1},
    });
}

Matrix *Matrix::rotationYMatrix(double rad) {
    return new Matrix(4, 4, {
            {cos(rad),  0, sin(rad), 0},
            {1,         0, 0,        0},
            {-sin(rad), 0, cos(rad), 0},
            {0,         0, 0,        1}
    });
}

Matrix *Matrix::rotationZMatrix(double rad) {
    return new Matrix(4, 4, {
            {cos(rad), -sin(rad), 0, 0},
            {sin(rad), cos(rad),  0, 0},
            {0,        0,         1, 0},
            {0,        0,         0, 1}
    });
}

Matrix *Matrix::shearingMatrix(double Xy, double Xz, double Yx, double Yz, double Zx, double Zy) {
    return new Matrix(4, 4, {
            {1,  Xy, Xz, 0},
            {Yx, 1,  Yz, 0},
            {Zx, Zy, 1,  0},
            {0,  0,  0,  1}
    });
}

Matrix Matrix::inverse() {
    return Matrix::inverse(*this);
}

Matrix Matrix::translate(double x, double y, double z) {
    return *Matrix::translationMatrix(x, y, z);
}

Matrix Matrix::scale(double x, double y, double z) {
    return *Matrix::scalingMatrix(x, y, z);
}

Matrix Matrix::rotateX(double rad) {
    return *Matrix::rotationXMatrix(rad);
}

Matrix Matrix::rotateY(double rad) {
    return *Matrix::rotationYMatrix(rad);
}

Matrix Matrix::rotateZ(double rad) {
    return *Matrix::rotationZMatrix(rad);
}

Matrix Matrix::shearing(double Xy, double Xz, double Yx, double Yz, double Zx, double Zy) {
    return *Matrix::shearingMatrix(Xy, Xz, Yx, Yz, Zx, Zy);
}

ostream &operator<<(ostream &out, const Matrix &m) {
    string str = "";
    for (int y = 0; y < m.values.size(); y++) {
        for (int x = 0; x < m.values.at(y).size(); x++) {
            out << m.at(y, x) << " ";
        }
        out << '\n';
    }
    return out;
}
