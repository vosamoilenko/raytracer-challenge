//
// Created by Volodymyr Samoilenko on 2019-07-06.
//

#ifndef RAYTRACER_CHALLENGE_COLOR_H
#define RAYTRACER_CHALLENGE_COLOR_H


#include "Tupel.h"

class Color: public Tupel {
    float RGB_MIN_VALUE = 0;
    float RGB_MAX_VALUE = 255;

public:
    Color();
    Color(const Tupel&);
    Color(double r, double g, double b);
    Color(vector<double>);
    const Color to255() const;
};


#endif //RAYTRACER_CHALLENGE_COLOR_H
