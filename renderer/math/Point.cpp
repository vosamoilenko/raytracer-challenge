//
// Created by Volodymyr Samoilenko on 2019-07-06.
//

#include "Point.h"

Point::Point() {
    values = vector<double>{0.0, 0.0, 0.0, 0.0};
}

Point::Point(const Tupel &t) {
    values = t.values;
    values.at(3) = 1.0;
}

Point::Point(double x, double y, double z) {
    values = vector<double>{x, y, z, 1.0};
}

Point::Point(vector<double> rhs) {
    if (rhs.size() != 3 && rhs.size() != 4) {
        string error = "point size is not 3 || 4, current size: " + to_string(rhs.size());
        throw runtime_error(error.c_str());
    }

    values.clear();
    values = vector(rhs.begin(), rhs.end());

    if (values.size() == 3) {
        // Marked as a point
        values.push_back(1);
    }

    if (values.size() != 4) {
        throw runtime_error("Tupel size is not 4");
    }
}

const Point Point::operator-(const Vector &rhs) const {
    Tupel temp = Tupel(*this) - Tupel(rhs);
    return Point(vector<double>{temp.x(), temp.y(), temp.z()});
}

const Vector Point::operator-(const Point &rhs) const {
    Tupel temp = Tupel(*this) - Tupel(rhs);
    return Vector(vector<double>{temp.x(), temp.y(), temp.z()});
}

const Point Point::operator+(const Vector &v) const {
    Tupel temp = Tupel(*this) + Tupel(v);
    return Point(temp.x(), temp.y(), temp.z());
}

ostream &operator<<(ostream &out, const Point &v) {
    string str =
            "(" + to_string(v.x()) + "," + to_string(v.y()) + "," + to_string(v.z()) + "," + to_string(v.w()) + ")";
    return out << str;
}