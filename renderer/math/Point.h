//
// Created by Volodymyr Samoilenko on 2019-07-06.
//

#ifndef RAYTRACER_CHALLENGE_POINT_H
#define RAYTRACER_CHALLENGE_POINT_H

#include <iostream>

#include "Tupel.h"
#include "Vector.h"


class Point : public Tupel {
public:


    Point();

    explicit Point(const Tupel &);

    Point(double x, double y, double z);

    Point(vector<double>);

    const Point operator-(const Vector &) const;

    const Vector operator-(const Point &) const;

    const Point operator+(const Vector &) const;
};

ostream &operator<<(ostream &out, const Point &v);

#endif //RAYTRACER_CHALLENGE_POINT_H
