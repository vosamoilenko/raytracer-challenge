//
// Created by Volodymyr Samoilenko on 2019-07-06.
//

#include <iostream>
#include <sstream>
#include <string>
#include <math.h>

#include "Tupel.h"

Tupel::Tupel()
{
    values = vector<double>{0.0, 0.0, 0.0, 0.0};
}

Tupel::Tupel(const Tupel &tuple)
{
    values.clear();
    values = tuple.values;
}

Tupel::Tupel(double x, double y, double z, double w)
{
    values = vector<double>{x, y, z, w};
}

Tupel::Tupel(vector<double> rhs)
{

    if (rhs.size() != 4)
    {
        throw runtime_error("Incoming tuple size is not 4");
    }
    values = vector(rhs.begin(), rhs.end());
}

void Tupel::operator=(const Tupel &tuple)
{
    values.clear();
    values = tuple.values;
}

Tupel::~Tupel() {}

const double Tupel::x() const
{
    return values.at(0);
}

const double Tupel::y() const
{
    return values.at(1);
}

const double Tupel::z() const
{
    return values.at(2);
}

const double Tupel::w() const
{
    return values.at(3);
}

const double Tupel::r() const
{
    return values.at(0);
}

const double Tupel::g() const
{
    return values.at(1);
}

const double Tupel::b() const
{
    return values.at(2);
}

Tupel Tupel::operator+(Tupel rhs) const
{
    double x, y, z, w;
    x = this->x() + rhs.x();
    y = this->y() + rhs.y();
    z = this->z() + rhs.z();
    w = this->w() + rhs.w();
    return Tupel(vector{x, y, z, w});
}

Tupel Tupel::operator-(Tupel rhs) const
{
    double x, y, z, w;
    x = this->x() - rhs.x();
    y = this->y() - rhs.y();
    z = this->z() - rhs.z();
    w = this->w() - rhs.w();
    return Tupel(vector{x, y, z, w});
}

Tupel Tupel::operator*(Tupel rhs) const
{
    double x, y, z, w;
    x = this->x() * rhs.x();
    y = this->y() * rhs.y();
    z = this->z() * rhs.z();
    w = this->w() * rhs.w();
    return Tupel(vector{x, y, z, w});
}

Tupel Tupel::operator/(Tupel rhs) const
{
    double x, y, z, w;
    x = this->x() / rhs.x();
    y = this->y() / rhs.y();
    z = this->z() / rhs.z();
    w = this->w() / rhs.w();
    return Tupel(vector{x, y, z, w});
}

Tupel Tupel::operator*(double rhs) const
{
    double x, y, z, w;
    x = this->x() * rhs;
    y = this->y() * rhs;
    z = this->z() * rhs;
    w = this->w() * rhs;
    return Tupel(vector{x, y, z, w});
}

Tupel Tupel::operator/(double rhs) const
{
    double x, y, z, w;
    x = this->x() / rhs;
    y = this->y() / rhs;
    z = this->z() / rhs;
    w = this->w() / rhs;
    return Tupel(vector{x, y, z, w});
}

const bool Tupel::isVector()
{
    return this->w() == 0;
}

const bool Tupel::isPoint()
{
    return this->w() == 1;
}

bool Tupel::operator==(vector<double> rhs) const
{
    float x = abs(this->x() - rhs.at(0));
    float y = abs(this->y() - rhs.at(1));
    float z = abs(this->z() - rhs.at(2));
    const float EPSILON = 0.00001;

    const bool isEqual = (x < EPSILON) &&
                         (y < EPSILON) &&
                         (z < EPSILON);

    if (this->values.size() != rhs.size())
    {
        if (rhs.size() == 3)
        {

            return isEqual;
        }
        else
        {
            return false;
        }
    }

    float w = abs(this->w() - rhs.at(3));
    return isEqual && (w < EPSILON);
}

bool Tupel::operator==(Tupel rhs) const
{
    return *this == rhs.values;
}

Tupel Tupel::operator-() const
{
    return Tupel(vector<double>{-this->x(), -this->y(), -this->z(), -this->w()});
}

double Tupel::magnitude() const
{
    return sqrt(pow(x(), 2) + pow(y(), 2) + pow(z(), 2) + pow(w(), 2));
}

double Tupel::length() const
{
    return magnitude();
}

double Tupel::length(const Tupel& t)
{
    return t.length();
}

Tupel Tupel::normalize() const
{
    const double l = length();
    return Tupel(x() / l,
                y() / l,
                z() / l,
                w() / l);
}

double Tupel::dot(const Tupel &rhs) const
{
    const Tupel product = *this * rhs;
    return product.x() + product.y() + product.z() + product.w();
}

double Tupel::dot(const Tupel &lhs, const Tupel &rhs)
{
    return lhs.dot(rhs);
}

// left or right coord system?
// https://stackoverflow.com/a/4820904
Tupel Tupel::cross(const Tupel &rhs)
{
    return Tupel(y() * rhs.z() - z() * rhs.y(),
                z() * rhs.x() - x() * rhs.z(),
                x() * rhs.y() - y() * rhs.x(),
                0.0);
}

double range(double target, double min, double max)
{
    if (target < min)
    {
        target = min;
    }
    else if (target > max)
    {
        target = max;
    }
    return target;
}

Tupel Tupel::toRange(double min, double max)
{
    return Tupel(range(this->x(), min, max), range(this->y(), min, max), range(this->z(), min, max), range(this->w(), min, max));
}

ostream &operator<<(ostream &out, const Tupel &tuple)
{
    ostringstream out_str;
    out_str << "(";
    int counter = 0;
    for (auto it = tuple.values.begin(); it != tuple.values.end(); it++)
    {
        out_str << *it;
        if (counter + 1 < tuple.values.size())
        {
            out_str << ", ";
        }
        counter += 1;
    }
    out_str << ")";
    return out << out_str.str();
}
