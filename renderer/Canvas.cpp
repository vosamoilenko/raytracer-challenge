//
// Created by Volodymyr Samoilenko on 2019-07-06.
//

#include "Canvas.h"

Canvas::Canvas(int w, int h) {
    this->width = w;
    this->height = h;
    grid = vector(this->height, vector<Color>(this->width));
}

const int Canvas::w() const {
    return this->width;
}

const int Canvas::h() const {
    return this->height;
}

void Canvas::write(int x, int y, Color c) {
    grid.at(y).at(x) = c;
}

const Color Canvas::at(int x, int y) const {
    return grid.at(y).at(x);
}

void Canvas::ppm(string filename) {
    string output_filename = filename + ".ppm";
    ofstream img(output_filename.c_str());

    img << "P3" << endl;
    img << width << " " << height << endl;
    img << "255" << endl;

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            Color c = this->at(x,y).to255();
            int r = c.r();
            int g = c.g();
            int b = c.b();
            img << r << " " << g << " " << b << endl;
        }
    }
}