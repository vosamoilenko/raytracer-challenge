//
// Created by Volodymyr Samoilenko on 2019-07-06.
//

#ifndef RAYTRACER_CHALLENGE_CANVAS_H
#define RAYTRACER_CHALLENGE_CANVAS_H


#include <vector>
#include <iostream>
#include <fstream>
#include <string>

#include "math/Color.h"

using namespace std;

class Canvas {
    int width, height;
public:
    vector<vector<Color>> grid;

    Canvas(int w, int h);
    const int w() const;
    const int h() const;

    void write(int x, int y, Color);
    const Color at(int x, int y) const;

    void ppm(string);
};


#endif //RAYTRACER_CHALLENGE_CANVAS_H
