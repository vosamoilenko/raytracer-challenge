//#define CATCH_CONFIG_MAIN
//#include "tests/catch.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>

#include "renderer/math/Matrix.h"
#include "renderer/Canvas.h"
#include "sketch/chapter6.h"

using namespace std;

 int main()
 {
   std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
   chapter6::run();
   std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
   std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << "[ns]" << std::endl;
   return 0;
 }