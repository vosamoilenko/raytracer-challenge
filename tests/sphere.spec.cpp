//
// Created by Volodymyr Samoilenko on 2019-07-19.
//

#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

#include "catch.h"

#include "../renderer/components/Sphere.h"
#include "../renderer/math/Matrix.h"

using namespace std;

// Scenario: A sphere's default transformation
// Given s ← sphere()
// Then s.transform = identity_matrix
SCENARIO("A sphere's default transformation", "[sphere]") {
    Sphere s;
    REQUIRE(*s.transformationMatrix == *Matrix::identity());
}

// Scenario: Changing a sphere's transformation
// Given s ← sphere()
// And t ← translation(2, 3, 4)
// When set_transform(s, t)
// Then s.transform = t
SCENARIO("Changing a sphere's transformation", "[sphere]") {
    Sphere s;
    Matrix *t = Matrix::translationMatrix(2, 3, 4);
    *s.transformationMatrix = *t;
    REQUIRE(*s.transformationMatrix == *t);
}

// Scenario: Intersecting a scaled sphere with a ray
// Given r ← ray(point(0, 0, -5), vector(0, 0, 1))
// And s ← sphere()
// When set_transform(s, scaling(2, 2, 2))
// And xs ← intersect(s, r)
// Then xs.count = 2
// And xs[0].t = 3
// And xs[1].t = 7
SCENARIO("Intersecting a scaled sphere with a ray", "[sphere][ray][intersection]") {
    Ray r(Point(0, 0, -5), Vector(0, 0, 1));
    Sphere s;
    s.transformationMatrix = Matrix::scalingMatrix(2, 2, 2);
    Intersections xs = r.intersect(s);
    REQUIRE(xs.size() == 2);
    REQUIRE(xs.at(0).t == 3);
    REQUIRE(xs.at(1).t == 7);
}

// Scenario: Intersecting a translated sphere with a ray
// Given r ← ray(point(0, 0, -5), vector(0, 0, 1))
// And s ← sphere()
// When set_transform(s, translation(5, 0, 0))
// And xs ← intersect(s, r) Then xs.count = 0
SCENARIO("Intersecting a translated sphere with a ray", "[sphere][ray][intersection]") {
    Ray r(Point(0, 0, -5), Vector(0, 0, 1));
    Sphere s;
    s.transformationMatrix = Matrix::translationMatrix(5, 0, 0);
    Intersections xs = r.intersect(s);
    REQUIRE(xs.size() == 0);
}


// Scenario: The normal on a sphere at a point on the x axis
// Given s ← sphere()
// When n ← normal_at(s, point(1, 0, 0))
// Then n = vector(1, 0, 0)
SCENARIO("The normal on a sphere at a point on the x axis", "[sphere][ray][intersection]") {
    Sphere s;
    Vector n = s.normalAt(Point(1, 0, 0));
    REQUIRE(n == Vector(1, 0, 0));
}

// Scenario: The normal on a sphere at a point on the y axis
// Given s ← sphere()
// When n ← normal_at(s, point(0, 1, 0))
// Then n = vector(0, 1, 0)
SCENARIO("The normal on a sphere at a point on the y axis", "[sphere][ray][intersection]") {
    Sphere s;
    Vector n = s.normalAt(Point(0, 1, 0));
    REQUIRE(n == Vector(0, 1, 0));
}

// Scenario: The normal on a sphere at a point on the z axis
// Given s ← sphere()
// When n ← normal_at(s, point(0, 0, 1))
// Then n = vector(0, 0, 1)
SCENARIO("The normal on a sphere at a point on the z axis", "[sphere][ray][intersection]") {
    Sphere s;
    Vector n = s.normalAt(Point(0, 0, 1));
    REQUIRE(n == Vector(0, 0, 1));
}

// Scenario: The normal on a sphere at a nonaxial point
// Given s ← sphere()
// When n ← normal_at(s, point(√3/3, √3/3, √3/3))
// Then n = vector(√3/3, √3/3, √3/3)
SCENARIO("The normal on a sphere at a nonaxial point", "[sphere][ray][intersection]") {
    Sphere s;
    Vector n = s.normalAt(Point(sqrt(3)/3, sqrt(3)/3, sqrt(3)/3));
    REQUIRE(n == Vector(sqrt(3)/3, sqrt(3)/3, sqrt(3)/3));
}

//  Scenario: The normal is a normalized vector
//  Given s ← sphere()
//  When n ← normal_at(s, point(√3/3, √3/3, √3/3))
//  Then n = normalize(n)
SCENARIO("The normal is a normalized vector", "[sphere][ray][intersection]") {
    Sphere s;
    Vector n = s.normalAt(Point(sqrt(3)/3, sqrt(3)/3, sqrt(3)/3));
    REQUIRE(n == n.normalize());
}

// Scenario: Computing the normal on a translated sphere
// Given s ← sphere()
// And set_transform(s, translation(0, 1, 0))
// When n ← normal_at(s, point(0, 1.70711, -0.70711))
// Then n = vector(0, 0.70711, -0.70711)
SCENARIO("Computing the normal on a translated sphere", "[sphere][matrix]") {
    Sphere s;
    s.transformationMatrix = Matrix::translationMatrix(0, 1, 0);
    Vector n = s.normalAt(Point(0, 1.70711, -0.70711));
    REQUIRE(n == Vector(0, 0.70711, -0.70711));
}

// Scenario: Computing the normal on a transformed sphere
// Given s ← sphere()
// And m ← scaling(1, 0.5, 1) * rotation_z(π/5)
// And set_transform(s, m)
// When n ← normal_at(s, point(0, √2/2, -√2/2))
// Then n = vector(0, 0.97014, -0.24254)
SCENARIO("Computing the normal on a transformed sphere", "[sphere][matrix]") {
    Sphere s;
    Matrix m = *Matrix::scalingMatrix(1, 0.5, 1) * *Matrix::rotationZMatrix(M_PI/5);
    s.transformationMatrix = &m;
    Vector n = s.normalAt(Point(0, sqrt(2)/2, -sqrt(2)/2));
    REQUIRE(n == Vector(0, 0.97014, -0.24254));
}
