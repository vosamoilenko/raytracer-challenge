//
// Created by Volodymyr Samoilenko on 2019-07-06.
//
#include <vector>

#include "catch.h"
#include "../renderer/math/Tupel.h"
#include "../renderer/math/Point.h"
#include "../renderer/math/Vector.h"
#include "../renderer/math/Color.h"
//#include "../renderer/math/utils.cpp"

using namespace std;

// Scenario: A tuple with w=1.0 is a point
// Given a ← tuple(4.3, -4.2, 3.1, 1.0)
// Then a.x = 4.3
// And a.y = -4.2
// And a.z = 3.1
// And a.w = 1.0
// And a is a point
// And a is not a vector
SCENARIO("A tuple with w=1.0 is a point", "[xyzw]") {
    GIVEN("a ← tuple(4.3, -4.2, 3.1, 1.0)") {
        Tupel a(4.3, -4.2, 3.1, 1.0);
        THEN("a.x = 4.3\na.y = -4.2\na.z = 3.1\na.w = 1.0") {
            REQUIRE(a.x() == 4.3);
            REQUIRE(a.y() == -4.2);
            REQUIRE(a.z() == 3.1);
            REQUIRE(a.w() == 1.0);
            REQUIRE(a.isPoint());
            REQUIRE(!a.isVector());
        }
    }
}

// Scenario: A tuple with w=0 is a vector
// Given a ← tuple(4.3, -4.2, 3.1, 0.0)
// Then a.x = 4.3
// And a.y = -4.2
// And a.z = 3.1
// And a.w = 0.0
// And a is not a point
// And a is a vector
SCENARIO("A tuple with w=0 is a vector", "[xyzw]") {
    GIVEN("a ← tuple(4.3, -4.2, 3.1, 0.0)") {
        Tupel a(4.3, -4.2, 3.1, 0.0);
        THEN("a.x = 4.3\na.y = -4.2\na.z = 3.1\na.w = 0.0") {
            REQUIRE(a.x() == 4.3);
            REQUIRE(a.y() == -4.2);
            REQUIRE(a.z() == 3.1);
            REQUIRE(a.w() == 0.0);
            REQUIRE(!a.isPoint());
            REQUIRE(a.isVector());
        }
    }
}

// Scenario: point() creates tuples with w=1
// Given p ← point(4, -4, 3)
// Then p = tuple(4, -4, 3, 1)
SCENARIO("point() creates tuples with w=1", "[xyzw],[Point]") {
    GIVEN("p ← point(4, -4, 3)") {
        Point p(4, -4, 3);
        THEN("p = tuple(4, -4, 3, 1)") {
            Tupel tuple(4, -4, 3, 1);
            REQUIRE(p == tuple);
        }
    }
}


// Scenario: vector() creates tuples with w=0
// Given v ← vector(4, -4, 3)
// Then v = tuple(4, -4, 3, 0)
SCENARIO("vector() creates tuples with w=0", "[xyzw],[Vector]") {
    GIVEN("p ← vector(4, -4, 3)") {
        Vector v(4, -4, 3);
        THEN("p = tuple(4, -4, 3, 0)") {
            Tupel tuple(4, -4, 3, 0);
            REQUIRE(v == tuple);
        }
    }
}

// Scenario: Adding two tuples
// Given a1 ← tuple(3, -2, 5, 1)
// And a2 ← tuple(-2, 3, 1, 0)
// Then a1 + a2 = tuple(1, 1, 6, 1)
SCENARIO("Adding two tuples", "[xyzw]") {
    GIVEN("a1 ← tuple(3, -2, 5, 1) and a2 ← tuple(-2, 3, 1, 0) ") {
        Tupel a1(3, -2, 5, 1);
        Tupel a2(-2, 3, 1, 0);
        THEN("a1 + a2 = tuple(1, 1, 6, 1)") {
            Tupel tuple(1, 1, 6, 1);
            REQUIRE((a1 + a2) == tuple);
        }
    }
}

// Scenario: Adding point and vector
SCENARIO("Adding point and vector", "[xyzw][point][vector]") {
    Point a(1, 1, 1);
    Vector v(1, 1, 1);
    Point p = a + v;
    REQUIRE(a + v == Point(2, 2, 2));
}


// Scenario: Subtracting two points
// Given p1 ← point(3, 2, 1)
// And p2 ← point(5, 6, 7)
// Then p1 - p2 = vector(-2, -4, -6)
SCENARIO("Subtracting two points", "[xyzw][Point]") {
    GIVEN("p1 ← point(3, 2, 1) and p2 ← point(5, 6, 7)") {
        Point p1(3, 2, 1);
        Point p2(5, 6, 7);
        THEN("p1 - p2 = vector(-2, -4, -6)") {
            Vector v = vector<double>{-2, -4, -6};
            Vector res = p1 - p2;
            REQUIRE(res == v);
        }
    }
}


// Scenario: Subtracting a vector from a point
// Given p ← point(3, 2, 1)
// And v ← vector(5, 6, 7)
// Then p - v = point(-2, -4, -6)
SCENARIO("Subtracting a vector from a point", "[xyzw][Vector]") {
    GIVEN("p ← point(3, 2, 1) and v ← vector(5, 6, 7)") {
        Point p(3, 2, 1);
        Vector v(5, 6, 7);
        THEN(" p - v = point(-2, -4, -6)") {
            Point res(-2, -4, -6);
            REQUIRE((p - v) == res);
        }
    }
}

// Scenario: Subtracting two vectors
// Given v1 ← vector(3, 2, 1)
// And v2 ← vector(5, 6, 7)
// Then v1 - v2 = vector(-2, -4, -6)
SCENARIO("Subtracting two vectors", "[xyzw][Vector]") {
    GIVEN("v1 ← vector(3, 2, 1) and v2 ← vector(5, 6, 7)") {
        Vector p(3, 2, 1);
        Vector v(5, 6, 7);
        THEN("v1 - v2 = vector(-2, -4, -6)") {
            Vector res(-2, -4, -6);
            REQUIRE((p - v) == res);
        }
    }
}

// Scenario: Subtracting a vector from the zero vector
// Given zero ← vector(0, 0, 0)
// And v ← vector(1, -2, 3)
// Then zero - v = vector(-1, 2, -3)
SCENARIO("Subtracting a vector from the zero vector", "[xyzw][Vector]") {
    GIVEN("zero ← vector(0, 0, 0) and v ← vector(1, -2, 3)") {
        Vector zero(0, 0, 0);
        Vector v(1, -2, 3);
        THEN("zero - v = vector(-1, 2, -3)") {
            Vector res(-1, 2, -3);
            REQUIRE((zero - v) == res);
        }
    }
}

// Scenario: Negating a tuple
// Given a ← tuple(1, -2, 3, -4)
// Then -a = tuple(-1, 2, -3, 4)
SCENARIO("Negating a tuple", "[xyzw][Vector]") {
    GIVEN("a ← tuple(1, -2, 3, -4)") {
        Tupel a(1, -2, 3, -4);
        THEN("-a = tuple(-1, 2, -3, 4)") {
            REQUIRE((-a) == Tupel(-1, 2, -3, 4));
        }
    }
}

// Scenario: Multiplying a tuple by a scalar
// Given a ← tuple(1, -2, 3, -4)
// Then a * 3.5 = tuple(3.5, -7, 10.5, -14)
// Then a * 0.5 = tuple(0.5, -1, 1.5, -2)
SCENARIO("Multiplying a tuple by a scalar", "[xyzw][Vector]") {
    GIVEN("a ← tuple(1, -2, 3, -4)") {
        Tupel a(1, -2, 3, -4);
        THEN("a * 3.5 = tuple(3.5, -7, 10.5, -14)") {
            REQUIRE((a * 3.5) == Tupel(3.5, -7, 10.5, -14));
            REQUIRE((a * 0.5) == Tupel(0.5, -1, 1.5, -2));
        }
    }
}

// Scenario: Dividing a tuple by a scalar
// Given a ← tuple(1, -2, 3, -4)
// Then a / 2 = tuple(0.5, -1, 1.5, -2)
SCENARIO("Dividing a tuple by a scalar", "[xyzw][Vector]") {
    GIVEN("a ← tuple(1, -2, 3, -4)") {
        Tupel a(1, -2, 3, -4);
        THEN("a / 2 = tuple(0.5, -1, 1.5, -2)") {
            REQUIRE((a / 2) == Tupel(0.5, -1, 1.5, -2));
        }
    }
}

// Scenario: Computing the magnitude of vectors
// Given v1 ← vector(1, 0, 0)
// Given v2 ← vector(1, 1, 0)
// Given v3 ← vector(1, 0, 1)
// Given v4 ← vector(1, 2, 3)
// Given v5 ← vector(-1, -2, -3)
// Then magnitude = 1
// Then magnitude = 1
// Then magnitude = 1
// Then magnitude = sqrt(14)
// Then magnitude = sqrt(14)
SCENARIO("Computing the magnitude of vector(1, 0, 0)", "[xyzw][Vector]") {
    GIVEN("vectors") {
        Vector v1(1, 0, 0);
        Vector v2(0, 1, 0);
        Vector v3(0, 0, 1);
        Vector v4(1, 2, 3);
        Vector v5(-1, -2, -3);
        THEN("1, 1, 1, sqrt(14), sqrt(14)") {
            REQUIRE(v1.length() == 1.0);
            REQUIRE(v2.length() == 1.0);
            REQUIRE(v3.length() == 1.0);
            REQUIRE(v4.length() == sqrt(14));
            REQUIRE(v5.length() == sqrt(14));
        }
    }
}



// Scenario : Normalizing vectors
// Given v ← vector(4, 0, 0)
// Then normalize(v) = vector(1, 0, 0)
// Given v ← vector(1, 2, 3)
// Then normalize(v) = approximately vector(0.26726, 0.53452, 0.80178)
// Given v ← vector(1, 2, 3)
// When norm ← normalize(v)
// Then magnitude(norm) = 1
SCENARIO("Normalizing vectors", "[xyzw][Vector]") {
    GIVEN("vectors") {
        Vector v1(4, 0, 0);
        Vector v2(1, 2, 3);
        Vector v3(1, 2, 3);
        THEN("1, 1, 1, sqrt(14), sqrt(14)") {
            REQUIRE(Vector(v1.normalize()) == Vector(1, 0, 0));
            REQUIRE(Vector(v2.normalize()) == Vector(0.267261, 0.534522, 0.801784));
            REQUIRE(v3.normalize().magnitude() == 1.0);
        }
    }
}

// Scenario: The dot product of two tuples
// Given a ← vector(1, 2, 3)
// And b ← vector(2, 3, 4)
// Then dot(a, b) = 20
SCENARIO("The dot product of two tuples", "[xyzw][Vector]") {
    GIVEN("a ← vector(1, 2, 3) and b ← vector(2, 3, 4)") {
        Vector a(1, 2, 3);
        Vector b(2, 3, 4);
        THEN("dot(a, b) = 20") {
            REQUIRE(a.dot(b) == 20);
        }
    }
}

//Scenario: The cross product of two vectors
//Given a ← vector(1, 2, 3)
//And b ← vector(2, 3, 4)
//Then cross(a, b) = vector(-1, 2, -1)
//And cross(b, a) = vector(1, -2, 1)
SCENARIO("The cross product of two vectors", "[xyzw][Vector]") {
    GIVEN("a ← vector(1, 2, 3) and b ← vector(2, 3, 4)") {
        Vector a(1, 2, 3);
        Vector b(2, 3, 4);
        THEN("cross(a, b) = vector(-1, 2, -1) and cross(b, a) = vector(1, -2, 1)") {
            REQUIRE(a.cross(b) == Vector(-1, 2, -1));
            REQUIRE(b.cross(a) == Vector(1, -2, 1));
        }
    }
}

// Scenario: Colors are (red, green, blue) tuples
// Given c ← color(-0.5, 0.4, 1.7)
// Then c.red = -0.5 And c.green = 0.4 And c.blue = 1.7
SCENARIO("Colors are (red, green, blue) tuples", "[xyzw][Vector]") {
    GIVEN("c ← color(-0.5, 0.4, 1.7)") {
        Color c(-0.5, 0.4, 1.7);
        THEN("Then c.red = -0.5 And c.green = 0.4 And c.blue = 1.7") {
            REQUIRE(c.r() == -0.5);
            REQUIRE(c.g() == 0.4);
            REQUIRE(c.b() == 1.7);
        }
    }
}

// TODO: refactor test case
//Scenario: Adding colors
//Given c1 ← color(0.9, 0.6, 0.75) And c2 ← color(0.7, 0.1, 0.25)
//Then c1 + c2 = color(1.6, 0.7, 1.0)
//Scenario: Subtracting colors
//Given c1 ← color(0.9, 0.6, 0.75)
//And c2 ← color(0.7, 0.1, 0.25)
//Then c1 - c2 = color(0.2, 0.5, 0.5)
//Scenario: Multiplying a color by a scalar
//Given c ← color(0.2, 0.3, 0.4)
//Then c * 2 = color(0.4, 0.6, 0.8)
//Scenario: Multiplying colors
//Given c1 ← color(1, 0.2, 0.4) And c2 ← color(0.9, 1, 0.1)
//Then c1 * c2 = color(0.9, 0.2, 0.04)
SCENARIO("Color basic operations", "[xyzw][Vector]") {
    GIVEN("c1 ← color(0.9, 0.6, 0.75) And c2 ← color(0.7, 0.1, 0.25)") {
        Color c1(0.9, 0.6, 0.75), c2(0.7, 0.1, 0.25);
        THEN("c1 + c2 = color(1.6, 0.7, 1.0), c1 - c2 = color(0.2, 0.5, 0.5), c * 2 = color(0.4, 0.6, 0.8)") {
            REQUIRE(c1 + c2 == Color(1.6, 0.7, 1.0));
            REQUIRE(c1 - c2 == Color(0.2, 0.5, 0.5));
            REQUIRE(Color(0.2, 0.3, 0.4) * 2 == Color(0.4, 0.6, 0.8));
            REQUIRE(Color(1, 0.2, 0.4) * Color(0.9, 1, 0.1) == Color(0.9, 0.2, 0.04));
        }
    }
}


// Scenario: Reflecting a vector approaching at 45°
// Given v ← vector(1, -1, 0)
// And n ← vector(0, 1, 0)
// When r ← reflect(v, n)
// Then r = vector(1, 1, 0)
SCENARIO("Reflecting a vector approaching at 45°", "[xyzw][Vector]") {
    Vector v(1, -1, 0);
    Vector n(0, 1, 0);
    Vector r = Vector::reflect(v, n);
    REQUIRE(r == Vector(1, 1, 0));
}

// Scenario: Reflecting a vector off a slanted surface
// Given v ← vector(0, -1, 0)
// And n ← vector(√2/2, √2/2, 0)
// When r ← reflect(v, n)
// Then r = vector(1, 0, 0)
SCENARIO(" Reflecting a vector off a slanted surface°", "[xyzw][Vector]") {
    Vector v(0, -1, 0);
    Vector n(sqrt(2) / 2, sqrt(2) / 2, 0);
    Vector r = Vector::reflect(v, n);
    REQUIRE(r == Vector(1, 0, 0));
}
