#include "catch.h"
#include "../renderer/components/Material.h"
#include "../renderer/components/Sphere.h"
#include "../renderer/components/Light.h"

// Scenario: The default material
// Given m ← material()
// Then m.color = color(1, 1, 1)
// And m.ambient = 0.1
// And m.diffuse = 0.9
// And m.specular = 0.9
// And m.shininess = 200.0
SCENARIO("The default material") {
    Material m;

    REQUIRE(m.ambient == 0.1);
    REQUIRE(m.diffuse == 0.9);
    REQUIRE(m.specular == 0.9);
    REQUIRE(m.shininess == 200.0);
}

// Scenario: A sphere has a default material
// Given s ← sphere()
// When m ← s.material
// Then m = material()
SCENARIO("A sphere has a default material") {
    Sphere s;
    Material m;
    REQUIRE(*s.material == m);
}

// Scenario: A sphere may be assigned a material
// Given s ← sphere()
// And m ← material()
// And m.ambient ← 1 When s.material ← m
// Then s.material = m
SCENARIO("A sphere may be assigned a material") {
    Sphere s;
    Material m;
    m.ambient = 1;
    s.material = &m;
    REQUIRE(*s.material == m);
}

// Background:
// Given m ← material()
// And position ← point(0, 0, 0)

// Scenario: Lighting with the eye between the light and the surface
// Given eyev ← vector(0, 0, -1)
// And normalv ← vector(0, 0, -1)
// And light ← point_light(point(0, 0, -10), color(1, 1, 1))
// When result ← lighting(m, light, position, eyev, normalv)
// Then result = color(1.9, 1.9, 1.9)
SCENARIO("Lighting with the eye between the light and the surface") {
    Material m;
    Point position(0, 0, 0);
    Vector eye(0, 0, -1);
    Vector normal(0, 0, -1);
    Light pointLight(Point(0, 0, -10), Color(1, 1, 1));
    Color color = Material::lightning(m, pointLight, position, eye, normal);
    REQUIRE(color == Color(1.9, 1.9, 1.9));

}

// Scenario: Lighting with the eye between light and surface, eye offset 45°
// Given eyev ← vector(0, √2/2, -√2/2)
// And normalv ← vector(0, 0, -1)
// And light ← point_light(point(0, 0, -10), color(1, 1, 1))
// When result ← lighting(m, light, position, eyev, normalv)
// Then result = color(1.0, 1.0, 1.0)
SCENARIO("Lighting with the eye between light and surface, eye offset 45°") {
    Material m;
    Point position(0, 0, 0);
    Vector eye(0, sqrt(2)/2, -sqrt(2)/2);
    Vector normal(0, 0, -1);
    Light pointLight(Point(0, 0, -10), Color(1, 1, 1));
    Color color = Material::lightning(m, pointLight, position, eye, normal);
    REQUIRE(color == Color(1.0, 1.0, 1.0));
}

// Scenario: Lighting with eye opposite surface, light offset 45°
// Given eyev ← vector(0, 0, -1)
// And normalv ← vector(0, 0, -1)
// And light ← point_light(point(0, 10, -10), color(1, 1, 1))
// When result ← lighting(m, light, position, eyev, normalv)
// Then result = color(0.7364, 0.7364, 0.7364)
SCENARIO("Lighting with eye opposite surface, light offset 45°") {
    Material m;
    Point position(0, 0, 0);
    Vector eye(0, 0, -1);
    Vector normal(0, 0, -1);
    Light pointLight(Point(0, 10, -10), Color(1, 1, 1));
    Color color = Material::lightning(m, pointLight, position, eye, normal);
    REQUIRE(color == Color(0.7364, 0.7364, 0.7364));
}

// Scenario: Lighting with eye in the path of the reflection vector
// Given eyev ← vector(0, -√2/2, -√2/2)
// And normalv ← vector(0, 0, -1)
// And light ← point_light(point(0, 10, -10), color(1, 1, 1))
// When result ← lighting(m, light, position, eyev, normalv)
// Then result = color(1.6364, 1.6364, 1.6364)
SCENARIO("Lighting with eye in the path of the reflection vector") {
    Material m;
    Point position(0, 0, 0);
    Vector eye(0, -sqrt(2)/2, -sqrt(2)/2);
    Vector normal(0, 0, -1);
    Light pointLight(Point(0, 10, -10), Color(1, 1, 1));
    Color color = Material::lightning(m, pointLight, position, eye, normal);
    REQUIRE(color == Color(1.6364, 1.6364, 1.6364));
}

// Scenario: Lighting with the light behind the surface
// Given eyev ← vector(0, 0, -1)
// And normalv ← vector(0, 0, -1)
// And light ← point_light(point(0, 0, 10), color(1, 1, 1))
// When result ← lighting(m, light, position, eyev, normalv)
// Then result = color(0.1, 0.1, 0.1)
SCENARIO("Lighting with the light behind the surface") {
    Material m;
    Point position(0, 0, 0);
    Vector eye(0, 0, -1);
    Vector normal(0, 0, -1);
    Light pointLight(Point(0, 0, 10), Color(1, 1, 1));
    Color color = Material::lightning(m, pointLight, position, eye, normal);
    REQUIRE(color == Color(0.1 ,0.1, 0.1));
}