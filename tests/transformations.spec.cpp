#include <vector>
#include <iostream>
#include <fstream>

#include "catch.h"
#include "../renderer/math/Matrix.h"
#include "../renderer/math/Point.h"

// TRANSFORM
// Scenario: Multiplying by a translation matrix
// Given transform ← translation(5, -3, 2)
// And p ← point(-3, 4, 5)
// Then transform * p = point(2, 1, 7)
SCENARIO("Multiplying by a translation matrix") {
    Matrix *translation = Matrix::translationMatrix(5, -3, 2);
    Point p(-3, 4, 5);
    REQUIRE(*translation * p == Point(2, 1, 7));
}

// Scenario: Multiplying by the inverse of a translation matrix
// Given transform ← translation(5, -3, 2)
// And inv ← inverse(transform)
// And p ← point(-3, 4, 5)
// Then inv * p = point(-8, 7, 3)
SCENARIO("Multiplying by the inverse of a translation matrix") {
    Matrix *translation = Matrix::translationMatrix(5, -3, 2);
    Matrix inv = Matrix::inverse(*translation);
    Point p(-3, 4, 5);
    REQUIRE(inv * p == Point(-8, 7, 3));
}

// Scenario: Translation does not affect vectors
// Given transform ← translation(5, -3, 2)
// And v ← vector(-3, 4, 5)
// Then transform * v = v
SCENARIO("Translation does not affect vectors") {
    Matrix *translation = Matrix::translationMatrix(5, -3, 2);
    Vector v(-3, 4, 5);
    REQUIRE(*translation * v == v);
}

// SCALING
// Scenario: A scaling matrix applied to a point Given transform ← scaling(2, 3, 4)
// And p ← point(-4, 6, 8)
// Then transform * p = point(-8, 18, 32)
SCENARIO("A scaling matrix applied to a point Given transform ← scaling(2, 3, 4)") {
    Matrix *transform = Matrix::scalingMatrix(2, 3, 4);
    Point p(-4, 6, 8);
    REQUIRE(*transform * p == Point(-8, 18, 32));
}

// Scenario: A scaling matrix applied to a vector Given transform ← scaling(2, 3, 4)
// And v ← vector(-4, 6, 8)
// Then transform * v = vector(-8, 18, 32)
SCENARIO("A scaling matrix applied to a vector Given transform ← scaling(2, 3, 4)") {
    Matrix *transform = Matrix::scalingMatrix(2, 3, 4);
    Vector p(-4, 6, 8);
    REQUIRE(*transform * p == Vector(-8, 18, 32));
}

// Scenario: Multiplying by the inverse of a scaling matrix Given transform ← scaling(2, 3, 4)
// And inv ← inverse(transform)
// And v ← vector(-4, 6, 8)
// Then inv * v = vector(-2, 2, 2)
SCENARIO("Multiplying by the inverse of a scaling matrix Given transform ← scaling(2, 3, 4)") {
    Matrix *transform = Matrix::scalingMatrix(2, 3, 4);
    Matrix inv = Matrix::inverse(*transform);
    Vector v(-4, 6, 8);
    REQUIRE(inv * v ==  Vector(-2, 2, 2));
}

// Scenario: Reflection is scaling by a negative value Given transform ← scaling(-1, 1, 1)
// And p ← point(2, 3, 4)
// Then transform * p = point(-2, 3, 4)
SCENARIO("Reflection is scaling by a negative value Given transform ← scaling(-1, 1, 1)") {
    Matrix *transform = Matrix::scalingMatrix(-1, 1, 1);
    Point p(2, 3, 4);
    REQUIRE(*transform * p == Point(-2, 3, 4));
}

// ROTATION X
// Scenario: Rotating a point around the x axis
// Given p ← point(0, 1, 0)
// And half_quarter ← rotation_x(π / 4)
// And full_quarter ← rotation_x(π / 2)
// Then half_quarter * p = point(0, √2/2, √2/2)
// And full_quarter * p = point(0, 0, 1)
SCENARIO("Rotating a point around the x axis Given p ← point(0, 1, 0)") {
   Point p(0, 1, 0);
   Matrix *hq = Matrix::rotationXMatrix(M_PI / 4);
   Matrix *fq = Matrix::rotationXMatrix(M_PI / 2);
   REQUIRE(*hq * p == Point(0, sqrt(2)/2.0, sqrt(2)/2.0));
   REQUIRE(*fq * p == Point(0, 0, 1));
}

// Scenario: The inverse of an x-rotation rotates in the opposite direction Given p ← point(0, 1, 0)
// And half_quarter ← rotation_x(π / 4)
// And inv ← inverse(half_quarter)
// Then inv * p = point(0, √2/2, -√2/2)
SCENARIO("The inverse of an x-rotation rotates in the opposite direction Given p ← point(0, 1, 0)") {
    Point p(0, 1, 0);
    Matrix *hq = Matrix::rotationXMatrix(M_PI / 4);
    Matrix inv = Matrix::inverse(*hq);
    REQUIRE(inv * p == Point(0, sqrt(2)/2.0, -sqrt(2)/2.0));
}

// ROTATION Y
// Scenario: Rotating a point around the y axis Given p ← point(0, 0, 1)
// And half_quarter ← rotation_y(π / 4)
// And full_quarter ← rotation_y(π / 2)
// Then half_quarter * p = point(√2/2, 0, √2/2)
// And full_quarter * p = point(1, 0, 0)
SCENARIO("Rotating a point around the y axis Given p ← point(0, 0, 1)") {
    Point p(0, 0, 1);
    Matrix hq = *Matrix::rotationYMatrix(M_PI / 4);
    Matrix fq = *Matrix::rotationYMatrix(M_PI / 2);
    REQUIRE(hq * p == Point(sqrt(2)/2.0, 0, sqrt(2)/2.0));
    REQUIRE(fq * p == Point(1, 0, 0));
}


// ROTATION Z
// Scenario: Rotating a point around the z axis Given p ← point(0, 1, 0)
// And half_quarter ← rotation_z(π / 4)
// And full_quarter ← rotation_z(π / 2)
// Then half_quarter * p = point(-√2/2, √2/2, 0)
// And full_quarter * p = point(-1, 0, 0)
SCENARIO("Rotating a point around the z axis Given p ← point(0, 1, 0)") {
    Point p(0, 1, 0);
    Matrix hq = *Matrix::rotationZMatrix(M_PI / 4);
    Matrix fq = *Matrix::rotationZMatrix(M_PI / 2);
    REQUIRE(hq * p == Point(-sqrt(2)/2.0, sqrt(2)/2.0, 0));
    REQUIRE(fq * p == Point(-1, 0, 0));
}

// SHEARING
// Scenario: A shearing transformation moves x in proportion to y
// Given transform ← shearing(1, 0, 0, 0, 0, 0)
// And p ← point(2, 3, 4)
// Then transform * p = point(5, 3, 4)
SCENARIO("A shearing transformation moves x in proportion to y Given transform ← shearing(1, 0, 0, 0, 0, 0)") {
    Matrix transformation = *Matrix::shearingMatrix(1, 0, 0, 0, 0, 0);
    Point p(2, 3, 4);
    REQUIRE(transformation * p == Point(5, 3, 4));
}

// Scenario: A shearing transformation moves x in proportion to z Given transform ← shearing(0, 1, 0, 0, 0, 0)
// And p ← point(2, 3, 4)
// Then transform * p = point(6, 3, 4)
SCENARIO("A shearing transformation moves x in proportion to z Given transform ← shearing(0, 1, 0, 0, 0, 0)") {
    Matrix transformation = *Matrix::shearingMatrix(0, 1, 0, 0, 0, 0);
    Point p(2, 3, 4);
    REQUIRE(transformation * p == Point(6, 3, 4));
}

// Scenario: A shearing transformation moves y in proportion to x Given transform ← shearing(0, 0, 1, 0, 0, 0)
// And p ← point(2, 3, 4)
// Then transform * p = point(2, 5, 4)
SCENARIO("A shearing transformation moves y in proportion to x Given transform ← shearing(0, 0, 1, 0, 0, 0)") {
    Matrix transformation = *Matrix::shearingMatrix(0, 0, 1, 0, 0, 0);
    Point p(2, 3, 4);
    REQUIRE(transformation * p == Point(2, 5, 4));
}

// Scenario: A shearing transformation moves y in proportion to z Given transform ← shearing(0, 0, 0, 1, 0, 0)
// And p ← point(2, 3, 4)
// Then transform * p = point(2, 7, 4)
SCENARIO("A shearing transformation moves y in proportion to z Given transform ← shearing(0, 0, 0, 1, 0, 0)") {
    Matrix transformation = *Matrix::shearingMatrix(0, 0, 0, 1, 0, 0);
    Point p(2, 3, 4);
    REQUIRE(transformation * p == Point(2, 7, 4));
}

// Scenario: A shearing transformation moves z in proportion to x Given transform ← shearing(0, 0, 0, 0, 1, 0)
// And p ← point(2, 3, 4)
// Then transform * p = point(2, 3, 6)
SCENARIO("A shearing transformation moves z in proportion to x Given transform ← shearing(0, 0, 0, 0, 1, 0)") {
    Matrix transformation = *Matrix::shearingMatrix(0, 0, 0, 0, 1, 0);
    Point p(2, 3, 4);
    REQUIRE(transformation * p == Point(2, 3, 6));
}

// Scenario: A shearing transformation moves z in proportion to y Given transform ← shearing(0, 0, 0, 0, 0, 1)
// And p ← point(2, 3, 4)
// Then transform * p = point(2, 3, 7)
SCENARIO("A shearing transformation moves z in proportion to y Given transform ← shearing(0, 0, 0, 0, 0, 1)") {
    Matrix transformation = *Matrix::shearingMatrix(0, 0, 0, 0, 0, 1);
    Point p(2, 3, 4);
    REQUIRE(transformation * p == Point(2, 3, 7));
}

// CHAINING TRANSFORMATION
// Scenario: Individual transformations are applied in sequence Given p ← point(1, 0, 1)
// And A ← rotation_x(π / 2)
// And B ← scaling(5, 5, 5)
// And C ← translation(10, 5, 7)
//   # apply rotation first
// When p2 ← A * p
// Then p2 = point(1, -1, 0) # then apply scaling
// When p3 ← B * p2
// Then p3 = point(5, -5, 0) # then apply translation
// When p4 ← C * p3
// Then p4 = point(15, 0, 7)
SCENARIO("Individual transformations are applied in sequence Given p ← point(1, 0, 1)") {
    Point p(1, 0, 1);
    Matrix a = *Matrix::rotationXMatrix(M_PI / 2.0);
    Matrix b = *Matrix::scalingMatrix(5, 5, 5);
    Matrix c = *Matrix::translationMatrix(10, 5, 7);

    Point p2 = a * p;
    REQUIRE(p2 == Point(1, -1, 0));
    Point p3 = b * p2;
    REQUIRE(p3 == Point(5, -5, 0));
    Point p4 = c * p3;
    REQUIRE(p4 == Point(15, 0, 7));
}

// Scenario: Chained transformations must be applied in reverse order Given p ← point(1, 0, 1)
// And A ← rotation_x(π / 2)
// And B ← scaling(5, 5, 5)
// And C ← translation(10, 5, 7)
// When T ← C * B * A
// Then T * p = point(15, 0, 7)
SCENARIO("Chained transformations must be applied in reverse order Given p ← point(1, 0, 1)") {
    Point p(1, 0, 1);
    Matrix a = *Matrix::rotationXMatrix(M_PI / 2.0);
    Matrix b = *Matrix::scalingMatrix(5, 5, 5);
    Matrix c = *Matrix::translationMatrix(10, 5, 7);
    Matrix t = c * b * a;

    REQUIRE(t * p == Point(15, 0, 7));
}

