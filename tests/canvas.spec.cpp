//
// Created by Volodymyr Samoilenko on 2019-07-06.
//


#include <vector>
#include <iostream>
#include <fstream>

#include "catch.h"
#include "../renderer/Canvas.h"
#include "../renderer/math/Color.h"

using namespace std;

// Scenario: Creating a canvas
// Given c ← canvas(10, 20)
// Then c.width = 10
// And c.height = 20
// And every pixel of c is color(0, 0, 0)
SCENARIO("Creating a canvas", "[canvas]") {
    GIVEN("c ← canvas(10, 20)") {
        Canvas c(10, 20);
        THEN("c.width = 10") {
            REQUIRE(c.w() == 10);
            THEN("c.height= 20") {
                REQUIRE(c.h() == 20);
                THEN("Every pixel is Color(0, 0, 0)") {
                    for (auto it = c.grid.begin(); it != c.grid.end(); it++) {
                        for (vector<Color>::iterator jt = it->begin(); jt != it->end(); jt++) {
                            Color c = *jt;
                            REQUIRE(c == Color(0, 0, 0));
                        }
                    }

                }
            }
        }
    }
}

//Scenario: Writing pixels to a canvas
//Given c ← canvas(10, 20)
//And red ← color(1, 0, 0)
//When write_pixel(c, 2, 3, red)
//Then pixel_at(c, 2, 3) = red
SCENARIO("Writing pixels to a canvas", "[canvas]") {
    GIVEN("c ← canvas(10, 20)") {
        Canvas c(10, 20);
        AND_GIVEN("red ← color(1, 0, 0) ") {
            Color red(1, 0, 0);
            WHEN("write_pixel(c, 2, 3, red)") {
                c.write(2, 3, red);
                THEN("pixel_at(c, 2, 3) = red") {
                    REQUIRE(c.at(2, 3) == red);
                }
            }
        }
    }
}

//Scenario: Constructing the PPM header
//Given c ← canvas(5, 3)
//When ppm ← canvas_to_ppm(c)
//Then line 1 of ppm are "Point"
//Then line 2 of ppm are "5 3"
//Then line 3 of ppm are "255"
SCENARIO("Constructing the PPM header", "[canvas]") {
    ifstream file;
    string filename = "canvas_header";
    GIVEN("c ← canvas(5, 3)") {
        Canvas c(5, 3);
        WHEN("ppm ← canvas_to_ppm(c)") {
            c.ppm(filename);
            THEN("Header is Point 5 3 255") {
                file.open ((filename + ".ppm").c_str());
                REQUIRE(file.is_open());
                string line;
                getline(file, line);
                REQUIRE(line == "P3");
                getline(file, line);
                REQUIRE(line == "5 3");
                getline(file, line);
                REQUIRE(line == "255");

                file.close();
                REQUIRE(remove((filename + ".ppm").c_str()) == 0);
            }
        }
    }
}

// projectile
// A projectile has a position (a point) and a velocity (a vector).
// An environment has gravity (a vector) and wind (a vector).


