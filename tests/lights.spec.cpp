#include "catch.h"
#include "../renderer/components/Light.h"

// Scenario: A point light has a position and intensity
// Given intensity ← color(1, 1, 1)
// And position ← point(0, 0, 0)
// When light ← point_light(position, intensity)
// Then light.position = position
// And light.intensity = intensity
SCENARIO("A point light has a position and intensity") {
    Light l(Point(0, 0, 0), Color(1,1,1));
    REQUIRE(l.position == Point(0,0,0));
    REQUIRE(l.intensity == Color(1,1,1));
}