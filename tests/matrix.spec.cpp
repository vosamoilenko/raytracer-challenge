#include <vector>
#include <iostream>
#include <fstream>

#include "catch.h"
#include "../renderer/math/Matrix.h"
#include "../renderer/math/Tupel.h"

using namespace std;

// Scenario: Constructing and inspecting a 4x4 matrix
// Given the following 4x4 matrix M:
//|   1  |   2  |   3  |   4  |
//|  5.5 |  6.5 |  7.5 |  8.5 |
//|   9  |  10  |  11  |  12  |
//| 13.5 | 14.5 | 15.5 | 16.5 |
// Then M[0,0] = 1
// And M[0,3] = 4
// And M[1,0] = 5.5
// And M[1,2] = 7.5
// And M[2,2] = 11
// And M[3,0] = 13.5
// And M[3,2] = 15.5
SCENARIO("Constructing and inspecting a 4x4 matrix", "[matrix]")
{
    GIVEN("following 4x4 matrix M")
    {
        Matrix *m = new Matrix(4, 4, {{1, 2, 3, 4}, {5.5, 6.5, 7.5, 8.5}, {9, 10, 11, 12}, {13.5, 14.5, 15.5, 16.5}});
        REQUIRE(m->at(0, 0) == 1);
        REQUIRE(m->at(0, 3) == 4);
        REQUIRE(m->at(1, 0) == 5.5);
        REQUIRE(m->at(1, 2) == 7.5);
        REQUIRE(m->at(2, 2) == 11);
        REQUIRE(m->at(3, 0) == 13.5);
        REQUIRE(m->at(3, 2) == 15.5);
    }
}

//Scenario: A 2x2 matrix ought to be representable
//Given the following 2x2 matrix M:
//| -3 | 5 |
//| 1 | -2 |
//Then M[0,0] = -3
//And M[0,1] = 5
// And M[1,0] = 1
//And M[1,1] = -2
SCENARIO("A 2x2 matrix ought to be representable", "[matrix]")
{
    GIVEN("the following 2x2 matrix M")
    {
        Matrix *m = new Matrix(2, 2, {{-3, 5}, {1, -2}});
        REQUIRE(m->at(0, 0) == -3);
        REQUIRE(m->at(0, 1) == 5);
        REQUIRE(m->at(1, 0) == 1);
        REQUIRE(m->at(1, 1) == -2);
    }
}

//Scenario: A 3x3 matrix ought to be representable
// Given the following 3x3 matrix M:
//|-3| 5| 0|
//| 1|-2|-7|
//| 0| 1| 1|
//Then M[0,0] = -3
//And M[1,1] = -2
// And M[2,2] = 1
SCENARIO("A 3x3 matrix ought to be representable", "[matrix]")
{
    GIVEN("the following 3x3 matrix M")
    {
        Matrix *m = new Matrix(3, 3, {{-3, 5, 0}, {1, -2, 7}, {0, 1, 1}});
        REQUIRE(m->at(0, 0) == -3);
        REQUIRE(m->at(1, 1) == -2);
        REQUIRE(m->at(2, 2) == 1);
    }
}

//Scenario: Matrix equality with identical matrices
//Given the following matrix A:
//|1|2|3|4|
//|5|6|7|8|
//|9|8|7|6|
//|5|4|3|2|
//And the following matrix B:
//|1|2|3|4|
//|5|6|7|8|
//|9|8|7|6|
//|5|4|3|2|
//Then A = B
SCENARIO("Matrix equality with identical matrices", "[matrix]")
{
    GIVEN("Given the following matrix A & B")
    {
        Matrix *m1 = new Matrix(4, 4, {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}});
        Matrix *m2 = new Matrix(4, 4, {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}});
        REQUIRE(*m1 == *m2);
    }
}

//Scenario: Matrix equality with different matrices
//Given the following matrix A:
//|1|2|3|4|
//|5|6|7|8|
//|9|8|7|6|
//|5|4|3|2|
//And the following matrix B:
//|2|2|3|4|
//|5|6|7|8|
//|9|8|7|6|
//|5|4|3|2|
//Then A != B
SCENARIO(" Matrix equality with different matrices", "[matrix]")
{
    GIVEN("Given the following matrix A & B")
    {
        Matrix *m1 = new Matrix(4, 4, {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}});
        Matrix *m2 = new Matrix(4, 4, {{2, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}});
        REQUIRE(*m1 != *m2);
    }
}

//Scenario: Multiplying two matrices Given the following matrix A:
//|1|2|3|4|
//|5|6|7|8|
//|9|8|7|6|
//|5|4|3|2|
//And the following matrix B:
//|-2|1|2| 3|
//| 3|2|1|-1|
//| 4|3|6| 5|
//| 1|2|7| 8|
//Then A * B is the following 4x4 matrix:
//|20| 22| 50| 48|
//|44| 54|114|108|
//|40| 58|110|102|
//|16| 26| 46| 42|
SCENARIO("Multiplying two matrices Given the following matrix A", "[matrix]")
{
    GIVEN("Given the following matrix A & B")
    {
        Matrix m1(4, 4, {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}});
        Matrix m2(4, 4, {{-2, 1, 2, 3}, {3, 2, 1, -1}, {4, 3, 6, 5}, {1, 2, 7, 8}});
        Matrix out(4, 4, {{20, 22, 50, 48}, {44, 54, 114, 108}, {40, 58, 110, 102}, {16, 26, 46, 42}});
        REQUIRE((m1 * m2) == out);
    }
}

// Scenario : A matrix multiplied by a tuple
// Given the following matrix A :
// | 1 | 2 | 3 | 4 |
// | 2 | 4 | 4 | 2 |
// | 8 | 6 | 4 | 1 |
// | 0 | 0 | 0 | 1 |
// And b ← tuple(1, 2, 3, 1)
// Then A *b = tuple(18, 24, 33, 1)
SCENARIO("A matrix multiplied by a tuple", "[matrix]")
{
    GIVEN("Given the following matrix A & tupel t")
    {
        Matrix a(4, 4, {{1, 2, 3, 4}, {2, 4, 4, 2}, {8, 6, 4, 1}, {0, 0, 0, 1}});
        Tupel t(1, 2, 3, 1);
        REQUIRE((a * t) == Tupel(18, 24, 33, 1));
    }
}

SCENARIO("A matrix multiplied by a point", "[matrix]")
{
    GIVEN("Given the following matrix A & point p")
    {
        Matrix a(4, 4, {{1, 2, 3, 4}, {2, 4, 4, 2}, {8, 6, 4, 1}, {0, 0, 0, 1}});
        Point p(1, 2, 3);
        REQUIRE((a * p) == Tupel(18, 24, 33, 1));
    }
}

SCENARIO("A matrix multiplied by a vector", "[matrix]")
{
    GIVEN("Given the following matrix A & vector v")
    {
        Matrix a(4, 4, {{1, 2, 3, 4}, {2, 4, 4, 2}, {8, 6, 4, 1}, {0, 0, 0, 1}});
        Vector v(1, 2, 3);
        REQUIRE((a * v) == Tupel(14, 22, 32, 0));
    }
}

//Scenario: Multiplying a matrix by the identity matrix
//Given the following matrix A:
//| 0 | 1 |  2 |  4 |
//| 1 | 2 |  4 |  8 |
//| 2 | 4 |  8 | 16 |
//| 4 | 8 | 16 | 32 |
//Then A * identity_matrix = A
//Scenario: Multiplying the identity matrix by a tuple
//Given a ← tuple(1, 2, 3, 4)
//Then identity_matrix * a = a
SCENARIO("Multiplying a matrix by the identity matrix", "[matrix]")
{
    GIVEN("the following matrix A")
    {
        Matrix a(4, 4, {{0, 1, 2, 4}, {1, 2, 4, 8}, {2, 4, 8, 15}, {4, 8, 16, 32}});
        REQUIRE((a * *Matrix::identity()) == a);
    }
}

// Scenario : Transposing a matrix
// Given the following matrix A :
// | 0 | 9 | 3 | 0 |
// | 9 | 8 | 0 | 8 |
// | 1 | 8 | 5 | 3 |
// | 0 | 0 | 5 | 8 |
// Then transpose(A) is the following matrix :
// | 0 | 9 | 1 | 0 |
// | 9 | 8 | 8 | 0 |
// | 3 | 0 | 5 | 5 |
// | 0 | 8 | 3 | 8 |
SCENARIO("Transposing a matrix", "[matrix]")
{
    GIVEN("the following matrix A")
    {
        Matrix a(4, 4, {{0, 9, 3, 0}, {9, 8, 0, 8}, {1, 8, 5, 3}, {0, 0, 5, 8}});
        Matrix t(4, 4, {{0, 9, 1, 0}, {9, 8, 8, 0}, {3, 0, 5, 5}, {0, 8, 3, 8}});
        REQUIRE(a.transponse() == t);
    }
}

//Scenario: Transposing the identity matrix
//Given A ← transpose(identity_matrix)
//Then A = identity_matrix
SCENARIO("Transposing the identity matrix", "[matrix]")
{
    GIVEN("the following matrix A")
    {
        REQUIRE(Matrix::identity()->transponse() == *Matrix::identity());
    }
}

//Scenario: Calculating the determinant of a 2x2 matrix
//Given the following 2x2 matrix A:
//| 1|5|
//| -3 | 2 |
//Then determinant(A) = 17
SCENARIO("Calculating the identity of 2x2 matrix", "[matrix]")
{
    GIVEN("the following 2x2 matrix A")
    {
        Matrix m(2, 2, {{1, 5}, {-3, 2}});
        REQUIRE(Matrix::determinant(m) == 17);
    }
}

//Scenario: A submatrix of a 3x3 matrix is a 2x2 matrix
//Given the following 3x3 matrix A:
//| 1|5| 0|
//|-3|2| 7|
//| 0|6|-3|
//Then submatrix(A, 0, 2) is the following 2x2 matrix:
//| -3 | 2 |
//|  0 | 6 |
SCENARIO("A submatrix of a 3x3 matrix is a 2x2 matrix", "[matrix]")
{
    GIVEN("the following 3x3 matrix A")
    {
        Matrix m(3, 3, {{1, 5, 0}, {-3, 2, 7}, {0, 6, -3}});
        Matrix out(2, 2, {{-3, 2}, {0, 6}});
        REQUIRE(Matrix::submatrix(0, 2, m) == out);
    }
}

//Scenario: A submatrix of a 4x4 matrix is a 3x3 matrix
//Given the following 4x4 matrix A:
//|-6| 1| 1| 6|
//|-8| 5| 8| 6|
//|-1| 0| 8| 2|
//|-7| 1|-1| 1|
//Then submatrix(A, 2, 1) is the following 3x3 matrix:
//| -6 |  1 | 6 |
//| -8 |  8 | 6 |
//| -7 | -1 | 1 |
SCENARIO("A submatrix of a 4x4 matrix is a 3x3 matrix", "[matrix]")
{
    GIVEN("Given the following 4x4 matrix A")
    {
        Matrix m(4, 4, {{-6, 1, 1, 6}, {-8, 5, 8, 6}, {-1, 0, 8, 2}, {-7, 1, -1, 1}});
        Matrix out(3, 3, {{-6, 1, 6}, {-8, 8, 6}, {-7, -1, 1}});
        REQUIRE(Matrix::submatrix(2, 1, m) == out);
    }
}

//Scenario: Calculating a minor of a 3x3 matrix
//Given the following 3x3 matrix A:
//| 3| 5| 0|
//| 2|-1|-7|
//| 6|-1| 5|
//And B ← submatrix(A, 1, 0)
//Then determinant(B) = 25
//And minor(A, 1, 0) = 25
SCENARIO("Calculating a minor of a 3x3 matrixx", "[matrix]")
{
    GIVEN("the following 3x3 matrix A")
    {
        Matrix m(3, 3, {{3, 5, 0}, {2, -1, -7}, {6, -1, 5}});
        AND_GIVEN("B ← submatrix(A, 1, 0)")
        {
            Matrix b = m.submatrix(1, 0);
            THEN("determinant(B) = 25")
            {
                REQUIRE(Matrix::determinant(b) == 25);
                AND_THEN("minor(A, 1, 0) = 25")
                {
                    REQUIRE(Matrix::minor(1, 0, m) == 25);
                }
            }
        }
    }
}

//Scenario: Calculating a cofactor of a 3x3 matrix
// Given the following 3x3 matrix A:
//| 3 |  5 | 0 |
//| 2 | -1 | -7|
//| 6 | -1 | 5 |
//Then minor(A, 0, 0) = -12
//And cofactor(A, 0, 0) = -12
//And minor(A, 1, 0) = 25
//And cofactor(A, 1, 0) = -25
SCENARIO("Calculating a cofactor of a 3x3 matrix", "[matrix]")
{
    Matrix m(3, 3, {{3, 5, 0}, {2, -1, -7}, {6, -1, 5}});
    REQUIRE(Matrix::minor(0, 0, m) == -12);
    REQUIRE(Matrix::cofactor(0, 0, m) == -12);
    REQUIRE(Matrix::minor(1, 0, m) == 25);
    REQUIRE(Matrix::cofactor(1, 0, m) == -25);
}

//Scenario: Calculating the determinant of a 3x3 matrix
//Given the following 3x3 matrix A:
//|1 | 2| 6|
//|-5| 8|-4|
//|2 | 6| 4|
//Then cofactor(A, 0, 0) = 56
//And cofactor(A, 0, 1) = 12
//And cofactor(A, 0, 2) = -46
//And determinant(A) = -196
SCENARIO("Calculating the determinant of a 3x3 matrix", "[matrix]")
{
    Matrix m(3, 3, {{1, 2, 6}, {-5, 8, -4}, {2, 6, 4}});
    REQUIRE(Matrix::cofactor(0, 0, m) == 56);
    REQUIRE(Matrix::cofactor(0, 1, m) == 12);
    REQUIRE(Matrix::cofactor(0, 2, m) == -46);
    REQUIRE(Matrix::determinant(m) == -196);
}

//Scenario: Calculating the determinant of a 4x4 matrix
//Given the following 4x4 matrix A:
//|-2|-8| 3| 5|
//|-3| 1| 7| 3|
//| 1| 2|-9| 6|
//|-6| 7| 7|-9|
//Then cofactor(A, 0, 0) = 690
//And cofactor(A, 0, 1) = 447
//And cofactor(A, 0, 2) = 210
//And cofactor(A, 0, 3) = 51
//And determinant(A) = -4071
SCENARIO("Calculating the determinant of a 4x4 matrix", "[matrix]")
{
    Matrix m(4, 4, {{-2, -8, 3, 5}, {-3, 1, 7, 3}, {1, 2, -9, 6}, {-6, 7, 7, -9}});
    REQUIRE(Matrix::cofactor(0, 0, m) == 690);
    REQUIRE(Matrix::cofactor(0, 1, m) == 447);
    REQUIRE(Matrix::cofactor(0, 2, m) == 210);
    REQUIRE(Matrix::cofactor(0, 3, m) == 51);
    REQUIRE(Matrix::determinant(m) == -4071);
}

// Scenario: Testing an invertible matrix for invertibility
// Given the following 4x4 matrix A:
// |6|4|4|4|
// |5|5|7|6|
// | 4|-9| 3|-7|
// | 9| 1| 7|-6|
// Then determinant(A) = -2120
// And A is invertible
SCENARIO("Testing an invertible matrix for invertibility", "[matrix]")
{
    Matrix m(4, 4, {{6, 4, 4, 4}, {5, 5, 7, 6}, {4, -9, 3, -7}, {9, 1, 7, -6}});
    REQUIRE(Matrix::determinant(m) == -2120);
    REQUIRE(Matrix::isInvertable(m) == true);
}

// Scenario: Testing a noninvertible matrix for invertibility
// Given the following 4x4 matrix A:
// |-4| 2|-2|-3|
// |9|6|2|6|
// | 0|-5| 1|-5|
// |0|0|0|0|
// Then determinant(A) = 0
// And A is not invertible
SCENARIO("Testing a noninvertible matrix for invertibility", "[matrix]")
{
    Matrix m(4, 4, {{-4, 2, -2, -3}, {9, 6, 2, 6}, {0, -5, 1, -5}, {0, 0, 0, 0}});
    REQUIRE(Matrix::determinant(m) == 0);
    REQUIRE(Matrix::isInvertable(m) == false);
}

// Scenario: Calculating the inverse of a matrix
// Given the following 4x4 matrix A:
// |-5| 2| 6|-8|
// | 1|-5| 1| 8|
// | 7| 7|-6|-7|
// | 1|-3| 7| 4|
// And B ← inverse(A)
// Then determinant(A) = 532
// And cofactor(A, 2, 3) = -160
// And B[3,2] = -160/532
// And cofactor(A, 3, 2) = 105
// And B[2,3] = 105/532
// And B is the following 4x4 matrix:
// | 0.21805 | 0.45113 | 0.24060 | -0.04511 |
// | -0.80827 | -1.45677 | -0.44361 | 0.52068 |
// | -0.07895 | -0.22368 | -0.05263 | 0.19737 |
// | -0.52256 | -0.81391 | -0.30075 | 0.30639 |
SCENARIO("Calculating the inverse of a matrix", "[matrix]")
{
    Matrix m(4, 4, {{-5, 2, 6, -8}, {1, -5, 1, 8}, {7, 7, -6, -7}, {1, -3, 7, 4}});
    Matrix inversed(4, 4, {{0.21805, 0.45113, 0.24060, -0.04511}, {-0.80827, -1.45677, -0.44361, 0.52068}, {-0.07895, -0.22368, -0.05263, 0.19737}, {-0.52256, -0.81391, -0.30075, 0.30639}});
    Matrix b = Matrix::inverse(m);
    REQUIRE(Matrix::determinant(m) == 532);
    REQUIRE(Matrix::cofactor(2, 3, m) == -160);
    REQUIRE(b.at(3, 2) == -160.0 / 532.0);
    REQUIRE(Matrix::cofactor(3, 2, m) == 105);
    REQUIRE(b.at(2, 3) == 105.0 / 532.0);
    REQUIRE(Matrix::inverse(m) == inversed);
}

// Scenario: Calculating the inverse of another matrix
// Given the following 4x4 matrix A:
// | 8|-5| 9| 2|
// |7|5|6|1|
// |-6| 0| 9| 6|
// |-3| 0|-9|-4|
// Then inverse(A) is the following 4x4 matrix:
// | -0.15385 | -0.15385 | -0.28205 | -0.53846 |
// | -0.07692 | 0.12308 | 0.02564 | 0.03077 |
// | 0.35897 | 0.35897 | 0.43590 | 0.92308 |
// | -0.69231 | -0.69231 | -0.76923 | -1.92308 |
SCENARIO("Calculating the inverse of another matrix", "[matrix]")
{
    Matrix m(4, 4, {{8, -5, 9, 2}, {7, 5, 6, 1}, {-6, 0, 9, 6}, {-3, 0, -9, -4}});
    Matrix out(4, 4, {{-0.15385, -0.15385, -0.28205, -0.53846}, {-0.07692, 0.12308, 0.02564, 0.03077}, {0.35897, 0.35897, 0.43590, 0.92308}, {-0.69231, -0.69231, -0.76923, -1.92308}});
    REQUIRE(Matrix::inverse(m) == out);
}

// Scenario: Calculating the inverse of a third matrix
// Given the following 4x4 matrix A:
// |9|3|0|9|
// | -5 | -2 | -6 | -3 |
// |-4| 9| 6| 4|
// |-7| 6| 6| 2|
// Then inverse(A) is the following 4x4 matrix:
// | -0.04074 | -0.07778 | 0.14444 | -0.22222 |
// | -0.07778 | 0.03333 | 0.36667 | -0.33333 |
// | -0.02901 | -0.14630 | -0.10926 | 0.12963 |
// | 0.17778 | 0.06667 | -0.26667 | 0.33333 |
SCENARIO("Calculating the inverse of a third matrix", "[matrix]")
{
    Matrix m(4, 4, {{9, 3, 0, 9}, {-5, -2, -6, -3}, {-4, 9, 6, 4}, {-7, 6, 6, 2}});
    Matrix out(4, 4, {{-0.04074, -0.07778, 0.14444, -0.22222}, {-0.07778, 0.03333, 0.36667, -0.33333}, {-0.02901, -0.14630, -0.10926, 0.12963}, {0.17778, 0.06667, -0.26667, 0.33333}});
    REQUIRE(Matrix::inverse(m) == out);
}

// Scenario: Multiplying a product by its inverse
// Given the following 4x4 matrix A:
// | 3|-9| 7| 3|
// | 3|-8| 2|-9|
// |-4| 4| 4| 1|
// |-6| 5|-1| 1|
// And the following 4x4 matrix B:
// |8|2|2|2|
// | 3|-1| 7| 0|
// |7|0|5|4|
// | 6|-2| 0| 5|
// And C ← A * B
// Then C * inverse(B) = A
SCENARIO("Multiplying a product by its inverse", "[matrix]")
{
    Matrix a(4, 4, {{3, -9, 7, 3}, {3, -8, 2, -9}, {-4, 4, 4, 1}, {-6, 5, -1, 1}});
    Matrix b(4, 4, {{8, 2, 2, 2}, {3, -1, 7, 0}, {7, 0, 5, 4}, {6, -2, 0, 5}});
    Matrix c = a * b;
    REQUIRE(c * Matrix::inverse(b) == a);
}

SCENARIO("test", "[matrix]") {
    Matrix ida(4, 4, {{5, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}});
    Tupel t(1,1,1,1);
    REQUIRE(ida * t == Tupel(5,1,1,1));
}
