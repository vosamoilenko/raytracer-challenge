//
// Created by Volodymyr Samoilenko on 2019-07-06.
//

#include "catch.h"
#include <vector>

using namespace std;

TEST_CASE("Concatenating two arrays should create a new array") {
    GIVEN("a ← array(1, 2, 3) and b ← array(3, 4, 5)") {
        vector<double> a{1, 2, 3};
        vector<double> b{3, 4, 5};
        vector<double> c;
        WHEN("c = a + b") {
            c.insert(c.end(), a.begin(), a.end());
            c.insert(c.end(), b.begin(), b.end());
            THEN("c = array(1, 2, 3, 3, 4, 5)") {
                REQUIRE(c == vector<double>{1, 2, 3, 3, 4, 5});
            }
        }
    }
}

