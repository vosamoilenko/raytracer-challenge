//
// Created by Volodymyr Samoilenko on 2019-07-19.
//

#include <vector>
#include <iostream>
#include <fstream>

#include "catch.h"
#include "../renderer/components/Ray.h"
#include "../renderer/components/Intersections.h"
#include "../renderer/math/Matrix.h"

using namespace std;

//Scenario: Creating and querying a ray
// Given origin ← point(1, 2, 3)
//And direction ← vector(4, 5, 6)
// When r ← ray(origin, direction)
// Then r.origin = origin
//And r.direction = direction
SCENARIO("Creating and querying a ray", "[ray]") {
    Vector direction(4, 5, 6);
    Point origin(1, 2, 3);
    Ray ray(origin, direction);
    REQUIRE(ray.direction == direction);
    REQUIRE(ray.origin == origin);
}

//Scenario: Computing a point from a distance
//Given r ← ray(point(2, 3, 4), vector(1, 0, 0))
// Then position(r, 0) = point(2, 3, 4)
//And position(r, 1) = point(3, 3, 4)
//And position(r, -1) = point(1, 3, 4)
// And position(r, 2.5) = point(4.5, 3, 4)
SCENARIO("Computing a point from a distance", "[ray]") {
    Ray ray(Point(2, 3, 4), Vector(1, 0, 0));
    REQUIRE(ray.position(0) == Point(2, 3, 4));
    REQUIRE(ray.position(1) == Point(3, 3, 4));
    REQUIRE(ray.position(-1) == Point(1, 3, 4));
    REQUIRE(ray.position(2.5) == Point(4.5, 3, 4));
}


//Scenario: A ray intersects a sphere at two points
//Given r ← ray(point(0, 0, -5), vector(0, 0, 1))
//And s ← sphere()
//When xs ← intersect(s, r)
// Then xs.count = 2
//And xs[0] = 4.0 And xs[1] = 6.0
SCENARIO("A ray intersects a sphere at two points", "[ray][sphere][intersection]") {
    Ray r(Point(0, 0, -5), Vector(0, 0, 1));
    Sphere sphere;
    Intersections intersectons = r.intersect(sphere);
    REQUIRE(intersectons.size() == 2);
    REQUIRE(intersectons.at(0).t == 4.0);
    REQUIRE(intersectons.at(1).t == 6.0);
}

//Scenario: A ray intersects a sphere at a tangent
//Given r ← ray(point(0, 1, -5), vector(0, 0, 1))
//And s ← sphere()
//When xs ← intersect(s, r)
// Then xs.count = 2
//And xs[0] = 5.0 And xs[1] = 5.0
SCENARIO("A ray intersects a sphere at a tangent", "[ray][sphere][intersection]") {
    Ray r(Point(0, 1, -5), Vector(0, 0, 1));
    Sphere sphere;
    Intersections intersections = r.intersect(sphere);
    REQUIRE(intersections.size() == 2);
    REQUIRE(intersections.at(0).t == 5.0);
    REQUIRE(intersections.at(1).t == 5.0);
}

//Scenario: A ray misses a sphere
//Given r ← ray(point(0, 2, -5), vector(0, 0, 1))
//And s ← sphere()
//When xs ← intersect(s, r)
// Then xs.count = 0
SCENARIO("A ray misses a sphere", "[ray][sphere][intersection]") {
    Ray r(Point(0, 2, -5), Vector(0, 0, 1));
    Sphere sphere;
    Intersections intersections = r.intersect(sphere);
    REQUIRE(intersections.size() == 0);
}

//Scenario: A ray originates inside a sphere
//Given r ← ray(point(0, 0, 0), vector(0, 0, 1))
//And s ← sphere()
//When xs ← intersect(s, r)
// Then xs.count = 2
//And xs[0] = -1.0 And xs[1] = 1.0
SCENARIO("A ray originates inside a sphere", "[ray][sphere][intersection]") {
    Ray r(Point(0, 0, 0), Vector(0, 0, 1));
    Sphere sphere;
    Intersections intersections = r.intersect(sphere);
    REQUIRE(intersections.size() == 2);
    REQUIRE(intersections.at(0).t == -1.0);
    REQUIRE(intersections.at(1).t == 1.0);
}

//Scenario: A sphere is behind a ray
//Given r ← ray(point(0, 0, 5), vector(0, 0, 1))
//And s ← sphere()
//When xs ← intersect(s, r)
// Then xs.count = 2
//And xs[0] = -6.0 And xs[1] = -4.0
SCENARIO("A sphere is behind a ray", "[ray][sphere][intersection]") {
    Ray r(Point(0, 0, 5), Vector(0, 0, 1));
    Sphere sphere;
    Intersections intersections = r.intersect(sphere);
    REQUIRE(intersections.size() == 2);
    REQUIRE(intersections.at(0).t == -6.0);
    REQUIRE(intersections.at(1).t == -4.0);
}

// Scenario: An intersection encapsulates t and object
// Given s ← sphere()
// When i ← intersection(3.5, s)
// Then i.t = 3.5
// And i.object = s
SCENARIO("An intersection encapsulates t and object", "[ray][sphere][intersection]") {
    Sphere sphere;
    Intersection intersection(3.5, &sphere);
    REQUIRE(intersection.t == 3.5);
    REQUIRE(intersection.object == &sphere);
}

// Scenario: Aggregating intersections
// Given s ← sphere()
// And i1 ← intersection(1, s)
// And i2 ← intersection(2, s)
// When xs ← intersections(i1, i2)
// Then xs.count = 2
// And xs[0].t = 1 And xs[1].t = 2
SCENARIO("Aggregating intersections", "[ray][sphere][intersection]") {
    Sphere sphere;
    Intersection i1(1, &sphere);
    Intersection i2(2, &sphere);
    Intersections intersections({i1, i2});
    REQUIRE(intersections.at(0).t == 1);
    REQUIRE(intersections[1].t == 2);
}

// Scenario: Intersect sets the object on the intersection
// Given r ← ray(point(0, 0, -5), vector(0, 0, 1))
// And s ← sphere()
// When xs ← intersect(s, r)
// Then xs.count = 2
// And xs[0].object = s And xs[1].object = s
SCENARIO("Intersect sets the object on the intersection", "[ray][sphere][intersection]") {
    Ray r(Point(0, 0, -5), Vector(0, 0, 1));
    Sphere sphere;
    Intersections intersections = r.intersect(sphere);
    REQUIRE(intersections.size() == 2);
    REQUIRE(intersections.at(0).object == &sphere);
    REQUIRE(intersections[1].object == &sphere);
}


// Scenario: The hit, when all intersections have positive t
// Given s ← sphere()
// And i1 ← intersection(1, s)
// And i2 ← intersection(2, s)
// And xs ← intersections(i2, i1)
// When i ← hit(xs) Then i = i1
SCENARIO("The hit, when all intersections have positive t", "[ray][sphere][intersection]") {
    Sphere s;
    Intersection i1(1, &s);
    Intersection i2(2, &s);
    Intersections xs({i1, i2});
    Intersection *i = Ray::hit(xs);
    REQUIRE(*i == i1);
}

// Scenario: The hit, when some intersections have negative t
// Given s ← sphere()
// And i1 ← intersection(-1, s)
// And i2 ← intersection(1, s)
// And xs ← intersections(i2, i1)
// When i ← hit(xs) Then i = i2
SCENARIO("The hit, when some intersections have negative t", "[ray][sphere][intersection]") {
    Sphere s;
    Intersection i1(-1, &s);
    Intersection i2(1, &s);
    Intersections xs({i1, i2});
    Intersection *i = Ray::hit(xs);
    REQUIRE(*i == i2);
}

// Scenario: The hit, when all intersections have negative t
// Given s ← sphere()
// And i1 ← intersection(-2, s)
// And i2 ← intersection(-1, s)
// And xs ← intersections(i2, i1)
// When i ← hit(xs) Then i is nothing
SCENARIO("The hit, when all intersections have negative t", "[ray][sphere][intersection]") {
    Sphere s;
    Intersection i1(-2, &s);
    Intersection i2(-1, &s);
    Intersections xs({i1, i2});
    Intersection *i = Ray::hit(xs);
    REQUIRE(i == nullptr);
}

// Scenario: The hit is always the lowest nonnegative intersection
// Given s ← sphere()
// And i1 ← intersection(5, s)
// And i2 ← intersection(7, s)
// And i3 ← intersection(-3, s)
// And i4 ← intersection(2, s)
// And xs ← intersections(i1, i2, i3, i4)
// When i ← hit(xs)
// Then i = i4
SCENARIO("The hit is always the lowest nonnegative intersection", "[ray][sphere][intersection]") {
    Sphere s;
    Intersection i1(5, &s);
    Intersection i2(7, &s);
    Intersection i3(-3, &s);
    Intersection i4(2, &s);
    Intersections xs({i1, i2, i3, i4});
    Intersection *i = Ray::hit(xs);
    REQUIRE(*i == i4);
}

// Scenario: Translating a ray
// Given r ← ray(point(1, 2, 3), vector(0, 1, 0))
// And m ← translation(3, 4, 5)
// When r2 ← transform(r, m)
// Then r2.origin = point(4, 6, 8)
// And r2.direction = vector(0, 1, 0)
SCENARIO("Translating a ray", "[ray][matrix]") {
    Ray r(Point(1, 2, 3), Vector(0, 1, 0));
    Matrix *m = Matrix::translationMatrix(3, 4, 5);
    Ray transformed = *m * r;
    REQUIRE(transformed.origin == Point(4, 6, 8));
    REQUIRE(transformed.direction == Vector(0, 1, 0));
}

SCENARIO("Translating a ray static", "[ray][matrix]") {
    Ray r(Point(1, 2, 3), Vector(0, 1, 0));
    Matrix *m = Matrix::translationMatrix(3, 4, 5);
    Ray *transformed = Ray::transform(r, *m);
    REQUIRE(transformed->origin == Point(4, 6, 8));
    REQUIRE(transformed->direction == Vector(0, 1, 0));
}

// Scenario: Scaling a ray
// Given r ← ray(point(1, 2, 3), vector(0, 1, 0))
// And m ← scaling(2, 3, 4)
// When r2 ← transform(r, m)
// Then r2.origin = point(2, 6, 12)
// And r2.direction = vector(0, 3, 0)
SCENARIO("Scaling a ray", "[ray][matrix]") {
    Ray r(Point(1, 2, 3), Vector(0, 1, 0));
    Matrix *m = Matrix::scalingMatrix(2, 3, 4);
    Ray transformed = *m * r;
    REQUIRE(transformed.origin == Point(2, 6, 12));
    REQUIRE(transformed.direction == Vector(0, 3, 0));
}

SCENARIO("Scaling a ray static", "[ray][matrix]") {
    Ray r(Point(1, 2, 3), Vector(0, 1, 0));
    Matrix *m = Matrix::scalingMatrix(2, 3, 4);
    Ray *transformed = Ray::transform(r, *m);
    REQUIRE(transformed->origin == Point(2, 6, 12));
    REQUIRE(transformed->direction == Vector(0, 3, 0));
}

// olol

